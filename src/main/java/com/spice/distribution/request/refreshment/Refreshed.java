package com.spice.distribution.request.refreshment;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author ankit
 *
 */
public class Refreshed implements Cloneable {
	@NotNull
	@JsonProperty
	private Integer item_seq_id;
	@NotNull
	@JsonProperty
	private Integer item_type_id;
	@NotNull
	@NotEmpty
	@JsonProperty
	private String item_resource_id;
	@NotNull
	@JsonProperty
	private Integer container_id;

	public Integer getItem_seq_id() {
		return item_seq_id;
	}

	public void setItem_seq_id(Integer item_seq_id) {
		this.item_seq_id = item_seq_id;
	}

	public Integer getItem_type_id() {
		return item_type_id;
	}

	public void setItem_type_id(Integer item_type_id) {
		this.item_type_id = item_type_id;
	}

	public String getItem_resource_id() {
		return item_resource_id;
	}

	public void setItem_resource_id(String item_resource_id) {
		this.item_resource_id = item_resource_id;
	}

	public Integer getContainer_id() {
		return container_id;
	}

	public void setContainer_id(Integer container_id) {
		this.container_id = container_id;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

}
