package com.spice.distribution.request;

import java.math.BigInteger;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "status", "message", "resourceCode", "refrenceId" })
public class CnemaNotificationRequest  implements Request{
	
	@JsonProperty("status")
	private String status;
	
	@NotNull
	@JsonProperty("message")
	private String message;
	
	@NotNull
	@JsonProperty("resourceCode")
	private String resourceCode;
	
	@NotNull
	@JsonProperty("refrenceId")
	private BigInteger refrenceId;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResourceCode() {
		return resourceCode;
	}

	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	public BigInteger getRefrenceId() {
		return refrenceId;
	}

	public void setRefrenceId(BigInteger refrenceId) {
		this.refrenceId = refrenceId;
	}
	
	

}
