/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.service;

import com.spice.distribution.exceptions.HttpException;
import com.spice.distribution.request.Request;
import com.spice.distribution.response.Response;

/**
 * @author ankit
 *
 */
public interface Http<T extends Request, R extends Response> {

	/**
	 * Send the Post request.
	 *
	 * @param t the t request parameter.
	 * @return the r response
	 * @throws HttpException the http exception if anything wrong occured.
	 */
	R post(T t) throws HttpException;
}
