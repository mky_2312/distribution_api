package com.spice.distribution.service.internal;

import java.util.List;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

/**
 * @author ankit
 *
 */
@Service
public class Excel {

	public Workbook buildExcelDocument(List<Object[]> artists) {
		XSSFWorkbook workbook = createBook();
		Sheet sheet = workbook.createSheet("Artists Details");
		sheet.setDefaultColumnWidth(30);
		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.BLUE.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		font.setBold(true);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);
		// create header row
		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("Id");
		header.getCell(0).setCellStyle(style);
		header.createCell(1).setCellValue("Full_Name");
		header.getCell(1).setCellStyle(style);
		header.createCell(2).setCellValue("Artist_Role");
		header.getCell(2).setCellStyle(style);
		header.createCell(3).setCellValue("Artist_Image_Resource_Code");
		header.getCell(3).setCellStyle(style);
		int rowCount = 1;
		for (Object[] object : artists) {
			Row userRow = sheet.createRow(rowCount++);
			userRow.createCell(0).setCellValue((Integer) object[0]);
			userRow.createCell(1).setCellValue((String) object[1]);
			userRow.createCell(2).setCellValue((String) object[2]);
			userRow.createCell(3).setCellValue((String) object[3]);
		}
		return workbook;
	}

	private XSSFWorkbook createBook() {
		return new XSSFWorkbook();
	}

}
