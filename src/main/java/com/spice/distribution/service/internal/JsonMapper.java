/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.service.internal;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.spice.distribution.response.Response;
import com.spice.distribution.service.Jsonify;

/**
 * The Class JsonMapper is used for managing services defined in Horizon.
 *
 * @author ankit
 */
@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "prototype")
public class JsonMapper<T, R extends Response> {

	
	public static final Integer KARAOKE = 999;
	
	/** The Constant BLAZE_ID representing SafariCom Service. */
	public static final Integer BLAZE_ID = 207;

	/** The Constant CNEMA_ID representing Cnema Service. */
	private static final Integer CNEMA_ID = 221;

	
	private static final Integer KEENG = 334;
	/**
	 * The Constant DEFAULT_ID. representing Common Service if none service is
	 * mentioned.
	 */
	
	private static final Integer DEFAULT_ID = 0;

	private static final Integer MUSICSCORER_ID=234;
	/** The map containing all the services. */
	private Map<Integer, Jsonify<Collection<T>, R>> map = new ConcurrentHashMap<>();

	/**
	 * Sets the json for safari com.
	 *
	 * @param t
	 * 
	 *            the new json for safari com
	 */
//	@Autowired
//	@Qualifier("safaricom")
//	private void setJsonForSafariCom(Jsonify<Collection<T>, R> t) {
//		map.put(BLAZE_ID, t);
//	}
	
	@Autowired
	@Qualifier("jsonkeeng")
	private void setJsonForKeeng(Jsonify<Collection<T>, R> t) {
		map.put(KEENG, t);
	}

	/**
	 * Sets the json for default.
	 *
	 * @param t the new json for default
	 */
	
	@Autowired
	@Qualifier("cmsjson")
	private void setJsonForSafariCom(Jsonify<Collection<T>, R> t) {
		map.put(BLAZE_ID, t);
	}
	
	
	
	@Autowired
	@Qualifier("cmsjson")
	private void setJsonForDefault(Jsonify<Collection<T>, R> t) {
		map.put(DEFAULT_ID, t);
	}
	
	/**
	 * Sets the json for default.
	 *
	 * @param t the new json for default
	 */
	@Autowired
	@Qualifier("karaokeJson")
	private void setJsonForKaraoke(Jsonify<Collection<T>, R> t) {
		map.put(KARAOKE, t);
	}

	/**
	 * Sets the json for cnema.
	 *
	 * @param t the new json for cnema
	 */
	@Autowired
	@Qualifier("cnemaJson")
	private void setJsonForCnema(Jsonify<Collection<T>, R> t) {
		map.put(CNEMA_ID, t);
	}

	@Autowired
	@Qualifier("musicscorerJson")
	private void setMusicScorer(Jsonify<Collection<T>, R> t) {
		map.put(MUSICSCORER_ID, t);
	}
	
	
	/**
	 * Gets the mapper.
	 *
	 * @param key the key
	 * @return the mapper representing json for particular service.
	 */
	public Jsonify<Collection<T>, R> getMapper(Integer key) {
		return doGetMapper(key);
	}

	/**
	 * Gets the default.
	 *
	 * @return the default mapper representing json if no service is found.
	 */
	private Jsonify<Collection<T>, R> getDefault() {
		return map.get(DEFAULT_ID);
	}

	//Client does not need to know
	private Jsonify<Collection<T>, R> doGetMapper(Integer key) {
		Jsonify<Collection<T>, R> response = map.get(key);
		if (Objects.isNull(response))
			return getDefault();
		return response;
	}
	
	public Jsonify<Collection<T>, R> getMapperKaraoke() {
		return doGetMapperKaraoke();
	}
	
	private Jsonify<Collection<T>, R> doGetMapperKaraoke() {
		Jsonify<Collection<T>, R> response = map.get(KARAOKE);
		if (Objects.isNull(response))
			return getDefault();
		return response;
	}
	
	
	public Jsonify<Collection<T>, R> getMapperKeeng() {
		return doGetMapperKeeng();
	}
	
	private Jsonify<Collection<T>, R> doGetMapperKeeng() {
		Jsonify<Collection<T>, R> response = map.get(KEENG);
		if (Objects.isNull(response))
			return getDefault();
		return response;
	}

}