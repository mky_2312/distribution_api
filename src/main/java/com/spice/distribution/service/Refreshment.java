/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.spice.distribution.cms.entity.ContentRefreshingTransactionDetails;
import com.spice.distribution.cms.entity.ContentRefreshingTransactionStatuses;
import com.spice.distribution.cms.entity.ContentRefreshingTransactions;
import com.spice.distribution.request.refreshment.Status;
import com.spice.distribution.request.refreshment.TransactionsRequest;
import com.spice.distribution.response.Respond;

/**
 * The Interface Refreshment for determining Contract for the Refreshement Application Programming Interface.
 *
 * @author ankit
 */
public interface Refreshment {

	/**
	 * Gets the transactions.
	 *
	 * @param <R> the generic type
	 * @param t the t
	 * @return the transactions
	 */
	<R> Page<R> getTransactions(Pageable t);
	
	<R> Page<R> getTransactionsMziiki(Pageable t);
	
	/**
	 * Gets the transactions.
	 *
	 * @param <R> the generic type
	 * @param t the t
	 * @param id the id
	 * @return the transactions
	 */	
	<R> Page<R> getTransactionsByUserId(int id,Pageable t);	

	/**
	 * Gets the transaction details with the given id.
	 *
	 * @param id the id
	 * @return the list representing ContentRefreshingTransactionDetails with the given param.
	 */
	List<ContentRefreshingTransactionDetails> getTransactionDetails(int id);

	/**
	 * Creates the new transaction.
	 *
	 * @param transaction the transaction to be created.
	 * @return the respond about the status of initiated transaction.
	 */
	Respond saveOrUpdate(ContentRefreshingTransactions transaction);

	/**
	 * Change status.
	 *
	 * @param status the status to be change
	 * @return the respond about the current status.
	 */
	Respond changeStatus(Status status);

	/**
	 * Gets the statuses.
	 *
	 * @return the statuses
	 */
	List<ContentRefreshingTransactionStatuses> getStatuses();

	/**
	 * Checks if is transaction active.
	 *
	 * @param request the request containing parameters for verification.
	 * @return the respond representing status of the transaction.
	 */
	Respond isTransactionActive(TransactionsRequest request);

	/**
	 * Search.
	 * @param text the text
	 *
	 * @param <T> the generic type
	 * @return the list
	 */
	<T> List<T> search(Class<T> clazz, String text, SimplePageable pageInfo, String... fields);

}
