/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.spice.distribution.cms.entity.ContentRefreshingTransactions;
import com.spice.distribution.service.impl.HttpClient;
import com.spice.distribution.util.RepositoryFactory;
import com.spice.distribution.util.Wait;

/**
 * The Class ContentRefreshmentThread base thread for making content live in Refreshment.
 *
 * @author ankit
 */
@Component
public class ContentRefreshmentThread extends Thread implements Wait {

	/** The Constant APPROVED. */
	public static final int APPROVED = 3;

	/** The Constant CONTENT_REFRESHING. */
	public static final int CONTENT_REFRESHING = 5;

	/** The Constant ALWAYS_RUNNING. */
	public static final boolean ALWAYS_RUNNING = true;

	/** The Constant ERROR_IN_CONTENT_REFRESHING. */
	public static final int ERROR_IN_CONTENT_REFRESHING = 6;

	/** The Constant CONTENT_REFRESHED. */
	public static final int CONTENT_REFRESHED = 7;

	private String baseUrl;
	private static final String uri = "api/refreshed/";

	/**
	 * Instantiates a new content refreshment thread.
	 *
	 * @param name the name
	 * @param factory the factory
	 * @param refreshed the refreshed
	 */
	@Autowired
	public ContentRefreshmentThread(@Value("${spice.horizon.refreshment.threadName}") String name, @Value("${spice.horizon.refreshment.refreshedRequest}") String baseUrl, RepositoryFactory factory) {
		super(name);
		this.factory = factory;
		this.baseUrl = baseUrl + uri;
		start();
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		while (ALWAYS_RUNNING) {
			wait(ONE_SEC);
			List<ContentRefreshingTransactions> transactions = factory.getContentRefreshingTransactionRepository().findByContentRefreshingTransactionStatusId(APPROVED);
			for (ContentRefreshingTransactions t : transactions) {
				HttpClient client = new HttpClient(baseUrl + t.getService_name());
				update(CONTENT_REFRESHING, t.getId());
				updateStatus(client.sendRequest(t), t.getId());
			}
		}
	}

	/**
	 * Waits the current thread .
	 *
	 * @param sec  no of seconds to be waited.
	 */
	public void wait(int sec) {
		try {
			Thread.sleep(sec);
		} catch (InterruptedException e) {
			throw new RuntimeException("Error in ContentRefreshmentThread : ", e);
		}
	}

	private void updateStatus(boolean status, int id) {
		if (status)
			update(CONTENT_REFRESHED, id);
		else
			update(ERROR_IN_CONTENT_REFRESHING, id);
	}

	private void update(int statusId, int id) {
		factory.getContentRefreshingTransactionRepository().updateStatus(statusId, id);
	}

	/** The factory. */
	private RepositoryFactory factory;

}
