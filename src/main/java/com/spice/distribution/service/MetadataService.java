package com.spice.distribution.service;

import com.spice.distribution.request.Request;
import com.spice.distribution.response.Response;

/**
 * @author ankit
 *
 */
public interface MetadataService<T, R> extends Request, Response {

	R processMetadataByResources(T t);
		
//	R updateRemarkByResources(T t);

	R processMetadataByReleases(T t);
	
	R processMetadataFeed(int t);
}
