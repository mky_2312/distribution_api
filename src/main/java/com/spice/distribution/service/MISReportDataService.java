package com.spice.distribution.service;


import java.util.List;

import com.spice.distribution.cms.entity.ServicePerformanceSummaries;
import com.spice.distribution.request.ServiceSummariesRequest;
import com.spice.distribution.response.Response;

public interface MISReportDataService  {

	public Response insertIntoServicePerformanceSummaries(ServiceSummariesRequest request,int service_id);
	
	public Response updateIntoServicePerformanceSummaries(ServiceSummariesRequest request,int service_id,ServicePerformanceSummaries obj);
	
	public List<ServicePerformanceSummaries> isServicePresent(ServiceSummariesRequest request,int service_id);
}
