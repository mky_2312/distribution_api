package com.spice.distribution.service.impl;

import com.spice.distribution.response.safaricom.AbrUrls;
import com.spice.distribution.response.safaricom.ArtworkImages;
import com.spice.distribution.response.safaricom.CarouselImages;
import com.spice.distribution.response.safaricom.HlsUrls;
import com.spice.distribution.response.safaricom.Mp3Urls;
import com.spice.distribution.response.safaricom.RtspUrls;

/**
 * @author ankit
 *
 */
public class JsonifySafariCom {
	protected static final Integer SKIZAID = 0;

	public ArtworkImages generateArtworkImages(String imageCode) {
		ArtworkImages artworkImages = new ArtworkImages();
		com.spice.distribution.response.urls.ArtworkImages images = new com.spice.distribution.response.urls.ArtworkImages(imageCode);
		artworkImages.setL(images.getL());
		artworkImages.setM(images.getM());
		artworkImages.setS(images.getS());
		artworkImages.setXs(images.getXs());
		return artworkImages;
	}

	public CarouselImages generateCarouselImages(String imageCode) {
		com.spice.distribution.response.urls.CarouselImages images = new com.spice.distribution.response.urls.CarouselImages(imageCode);
		CarouselImages carouselImages = new CarouselImages();
		carouselImages.setL(images.getL());
		carouselImages.setM(images.getM());
		carouselImages.setS(images.getS());
		carouselImages.setXs(images.getXs());
		return carouselImages;
	}

	public AbrUrls generateAbrUrls(String resourceCode) {
		com.spice.distribution.response.urls.AbrUrls abr = new com.spice.distribution.response.urls.AbrUrls();
		abr.setAbrUrls(resourceCode);
		AbrUrls abrUrls = new AbrUrls();
		abrUrls.setAuto(abr.getAuto());
		abrUrls.setHigh(abr.getHigh());
		abrUrls.setLow(abr.getLow());
		abrUrls.setMedium(abr.getMedium());
		return abrUrls;

	}

	public HlsUrls generateHls(String resourceCode) {
		com.spice.distribution.response.urls.HlsUrls hls = new com.spice.distribution.response.urls.HlsUrls();
		hls.setHlsUrls(resourceCode);
		HlsUrls hlsUrls = new HlsUrls();
		hlsUrls.setAuto(hls.getAuto());
		hlsUrls.setHigh(hls.getHigh());
		hlsUrls.setLow(hls.getLow());
		hlsUrls.setMedium(hls.getMedium());
		return hlsUrls;
	}

	public Mp3Urls generateMp3(String resourceCode) {
		com.spice.distribution.response.urls.Mp3Urls mp3 = new com.spice.distribution.response.urls.Mp3Urls();
		mp3.setMp3Urls(resourceCode);
		Mp3Urls mp3Urls = new Mp3Urls();
		mp3Urls.setAuto(mp3.getAuto());
		mp3Urls.setHigh(mp3.getHigh());
		mp3Urls.setLow(mp3.getLow());
		mp3Urls.setMedium(mp3.getMedium());
		return mp3Urls;
	}

	public RtspUrls generateRtsp(String resourceCode) {
		com.spice.distribution.response.urls.RtspUrls rtsp = new com.spice.distribution.response.urls.RtspUrls();
		rtsp.setRtspUrls(resourceCode);
		RtspUrls rtspUrls = new RtspUrls();
		rtspUrls.setAuto(rtsp.getAuto());
		rtspUrls.setHigh(rtsp.getHigh());
		rtspUrls.setLow(rtsp.getLow());
		rtspUrls.setMedium(rtsp.getMedium());
		return rtspUrls;
	}
}
