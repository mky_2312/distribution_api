package com.spice.distribution.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import com.spice.distribution.util.ObjectsUtil;
import com.spice.distribution.cms.entity.ReleaseResourceTags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import com.spice.distribution.util.RepositoryFactory;
import com.google.common.base.Joiner;
import com.spice.distribution.cms.constant.ResourceType;
import com.spice.distribution.cms.entity.ContentPartners;
import com.spice.distribution.cms.entity.Images;
import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.cms.entity.Releases;
import com.spice.distribution.cms.entity.SoundRecordings;
import com.spice.distribution.cms.entity.TechnicalImageFileDetails;
import com.spice.distribution.cms.entity.TechnicalSoundRecordingFileDetails;
import com.spice.distribution.cms.repo.ArtistRolesRepository;
import com.spice.distribution.cms.repo.ContentPartnersRepository;
import com.spice.distribution.cms.repo.DealRepository;
import com.spice.distribution.cms.repo.DisplayArtistRepository;
import com.spice.distribution.cms.repo.GenresRepository;
import com.spice.distribution.cms.repo.ReleaseResourcesRepository;
import com.spice.distribution.cms.repo.ReleasesRepository;
import com.spice.distribution.response.metadata.DisplayArtist;
import com.spice.distribution.response.metadata.Genre;
import com.spice.distribution.response.metadata.MetadataResponse;
import com.spice.distribution.response.metadata.Release;
import com.spice.distribution.response.metadata.ResourceList;
import com.spice.distribution.response.metadata.TechnicalImageFileDetail;
import com.spice.distribution.response.metadata.TechnicalSoundRecordingFileDetail;
import com.spice.distribution.service.Jsonify;
import com.spice.distribution.service.internal.CommonJson;
import com.spice.distribution.util.Filter;

/**
 * @author ankit
 *
 */
@Component("cmsjson")
@Scope(proxyMode = ScopedProxyMode.INTERFACES, value = "prototype")
public class JsonifyCMS extends CommonJson implements Jsonify<Collection<ReleaseResources>, MetadataResponse> {

	@Override
	public MetadataResponse generate(Collection<ReleaseResources> releaseResourcesIterator,int serviceId) {
		Objects.requireNonNull(releaseResourcesIterator, "ReleaseResources Not Found");
		return this.generateMetdataResponse(releaseResourcesIterator.iterator());
	}
	
	@Override
	public MetadataResponse generateKaraoke(Collection<ReleaseResources> releaseResourcesIterator) {
		Objects.requireNonNull(releaseResourcesIterator, "ReleaseResources Not Found");
		return this.generateMetdataResponse(releaseResourcesIterator.iterator());
	}

	public MetadataResponse generateForRelease(Iterable<Releases> releasesIterable) {
		Objects.requireNonNull(releasesIterable, "Releases Not Found");
		return this.generateMetadataResponseForRelease(releasesIterable);
	}

	public MetadataResponse generateMetdataResponse(Iterator<ReleaseResources> iterator) {
		MetadataResponse response = new MetadataResponse();
		response.setResourceList(this.generateResourceList(iterator));
		return response;
	}

	public MetadataResponse generateMetadataResponseForRelease(Iterable<Releases> releasesIterable) {
		MetadataResponse response = new MetadataResponse();
		response.setResourceList(this.generateResourceListForReleases(releasesIterable));
		return response;
	}

	public List<ResourceList> generateResourceList(Iterator<ReleaseResources> iterator) {
		List<ResourceList> resourceList = new ArrayList<>();
		while (iterator.hasNext()) {
			ReleaseResources rr = iterator.next();
			resourceList.add(this.generateResource(rr));
		}

		return resourceList;
	}

	public List<ResourceList> generateResourceListForReleases(Iterable<Releases> releasesIterable) {
		Iterable<ReleaseResources> releaseResourcesIterable = releaseResourcesRepository.findByReleaseIdIn(Filter.filterReleaseId((Collection<Releases>) releasesIterable));
		List<ResourceList> resourceList = new ArrayList<>();
		Iterator<ReleaseResources> iterator = releaseResourcesIterable.iterator();
		while (iterator.hasNext()) {
			ReleaseResources rr = iterator.next();
			resourceList.add(this.generateResource(rr));
		}
		return resourceList;
	}

	public ResourceList generateResource(ReleaseResources rr) {
		ResourceList resourceList = new ResourceList();
		resourceList.setResourceCode(rr.getResourceCode());
		// Optional<SoundRecordings> optional =
		// Filter.findDurationForCMS(rr.getSoundRecordings());
		// if (optional.isPresent()) {
		// SoundRecordings sr = optional.get();
		// if (sr.getDuration() != null)
		// resourceList.setDuration(sr.getDuration().intValue());
		// }
		if (ResourceType.SoundRecording.getId() == rr.getResource_type_id()) {
			resourceList.setResourceType(ResourceType.SoundRecording.name());
			for (SoundRecordings recordings : rr.getSoundRecordings()) {
				resourceList.setISRC(recordings.getIsrc());
				resourceList.setTitle(recordings.getTitle_text());
				resourceList.setSubTitle(recordings.getSub_title());
				resourceList.setTechnicalSoundRecordingFileDetails(this.generateSoundFileDetails(recordings.getTechnicalSoundRecordingFileDetails(), resourceList));
			}
		} else if (ResourceType.Image.getId() == rr.getResource_type_id()) {
			resourceList.setResourceType(ResourceType.Image.name());
			for (Images images : rr.getImages()) {
				resourceList.setTitle(images.getTitle_text());
				resourceList.setSubTitle(images.getSub_title());
			}
		}
		ContentPartners contentPartners = repo.findOne(rr.getContent_partner_id());
		resourceList.setResourceTags(createTag(rr));
		resourceList.setContentPartner(contentPartners.getContent_partner());
		resourceList.setCountryOfOrigin(this.generateCountryOfOrigin(contentPartners.getCountries()));
		Releases releases = releasesRepository.findOne(rr.getReleaseId());
		Release release = this.generateReleases(rr, releases);
		this.generateTDG(rr, resourceList, releases.getId());
		List<ReleaseResources> releaseResourcesImageCode = releaseResourcesRepository.findByReleaseId(releases.getId());
		for (ReleaseResources imageCode : releaseResourcesImageCode) {
			if (imageCode.getResource_type_id().equals(ResourceType.Image.getId())) {
				resourceList.setResourceImageCode(imageCode.getResourceCode());
				release.setReleaseImageCode(imageCode.getResourceCode());
				if (Objects.nonNull(resourceList.getTechnicalImageFileDetails()))
					resourceList.getTechnicalImageFileDetails().addAll(this.generateImageFileDetails(imageCode.getImages().iterator().next().getTechnicalImageFileDetails()));
				else
					resourceList.setTechnicalImageFileDetails(this.generateImageFileDetails(imageCode.getImages().iterator().next().getTechnicalImageFileDetails()));
				break;
			}
		}
		resourceList.setRelease(release);
		if (message.isEmpty()) {
			resourceList.setStatus(true);
			resourceList.setMessage("Successfull");
		} else {
			resourceList.setMessage(Joiner.on(",").join(message));
			message.clear();
		}
		if (Objects.isNull(resourceList.getDuration()))
			resourceList.setDuration(new Integer(0));
		return resourceList;
	}

	public List<TechnicalImageFileDetail> generateImageFileDetails(List<TechnicalImageFileDetails> entityList) {
		List<TechnicalImageFileDetail> detailsList = new ArrayList<>();
		for (TechnicalImageFileDetails details : entityList) {
			TechnicalImageFileDetail detail = new TechnicalImageFileDetail();
			detail.setCodec(details.getTechnicalImageDetails().getImage_codec_type());
			if (Objects.nonNull(details.getTechnicalImageDetails().getImage_height().intValue()))
				detail.setHeight(details.getTechnicalImageDetails().getImage_height().intValue());
			if (Objects.nonNull(details.getTechnicalImageDetails().getImage_width().intValue()))
				detail.setWidth(details.getTechnicalImageDetails().getImage_width().intValue());
			detail.setFilePath(details.getFile_path());
			detail.setFileName(details.getFile_name());
			detailsList.add(detail);
		}
		return detailsList;
	}

	public List<TechnicalSoundRecordingFileDetail> generateSoundFileDetails(List<TechnicalSoundRecordingFileDetails> entityList, ResourceList resourceList) {
		List<TechnicalSoundRecordingFileDetail> detailsList = new ArrayList<>();
		for (TechnicalSoundRecordingFileDetails details : entityList) {
			TechnicalSoundRecordingFileDetail detail = new TechnicalSoundRecordingFileDetail();
			if (Objects.nonNull(details.getDuration())) {
				detail.setDuration(details.getDuration().intValue());
				if (resourceList.getDuration() != null && resourceList.getDuration() < detail.getDuration())
					resourceList.setDuration(detail.getDuration());
				else if (resourceList.getDuration() == null)
					resourceList.setDuration(detail.getDuration());
			}
			if (Objects.nonNull(details.getFile_size()))
				detail.setFileSize(details.getFile_size().intValue());
			detail.setFileName(details.getFile_name());
			detail.setFilePath(details.getFile_path());
			detail.setCodec(details.getTechnicalSoundRecordingDetails().getAudio_codec_type());
			if (Objects.nonNull(details.getTechnicalSoundRecordingDetails().getSampling_rate()))
				detail.setSamplingRate(details.getTechnicalSoundRecordingDetails().getSampling_rate().intValue());
			if (Objects.nonNull(details.getTechnicalSoundRecordingDetails().getBit_rate()))
				detail.setBitRate(details.getTechnicalSoundRecordingDetails().getBit_rate().intValue());
			if (Objects.nonNull(details.getTechnicalSoundRecordingDetails().getNumber_of_channels()))
				detail.setNumberOfChannels(details.getTechnicalSoundRecordingDetails().getNumber_of_channels().toString());
			detailsList.add(detail);
		}
		return detailsList;
	}

	public Release generateReleases(ReleaseResources rr, Releases releases) {
		Release release = new Release();
		release.setTitle(releases.getTitle_text());
		release.setSubTitle(releases.getSub_title());
		release.setUPC(releases.getUpc());
		release.setReleaseType(releases.getRelease_type().getRelease_type());
		return release;
	}

	public void generateTDG(ReleaseResources rr, ResourceList resourceList, int id) {
		resourceList.setTerritoryCodes(this.generateTerritoryCodes(dealRepository.findByReleaseId(id)));
		resourceList.setDisplayArtists(this.generateDisplayArtist(rr, resourceList));
		resourceList.setGenres(this.generateGenre(rr, resourceList));
	}

	public List<Genre> generateGenre(ReleaseResources rr, ResourceList rl) {
		List<Genre> genreList = new ArrayList<>();
		List<Object[]> genres = genresRepository.findByReleaseResourceId(rr.getId());
		for (Object[] obj : genres) {
			Genre genre = new Genre();
			genre.setGenre((String) obj[0]);
			String imageCode = (String) obj[1];
			if (Objects.nonNull(imageCode)) {
				genre.setGenreImageCode(imageCode);
				ReleaseResources releaseResource = releaseResourcesRepository.findByResourceCode(imageCode);
				if (Objects.nonNull(rl.getTechnicalImageFileDetails()))
					rl.getTechnicalImageFileDetails().addAll(this.generateImageFileDetails(releaseResource.getImages().iterator().next().getTechnicalImageFileDetails()));
				else
					rl.setTechnicalImageFileDetails(this.generateImageFileDetails(releaseResource.getImages().iterator().next().getTechnicalImageFileDetails()));
			}
			genreList.add(genre);
		}
		if (Objects.isNull(genreList) && genreList.isEmpty())
			message.add("Genre Not Found");
		return genreList;
	}

	public List<DisplayArtist> generateDisplayArtist(ReleaseResources rr, ResourceList rl) {
		List<DisplayArtist> displayArtistList = new ArrayList<>();
		List<Object[]> displayArtistName = displayArtistRepository.findByReleaseResourceId(rr.getId());
		List<String> artistRole = artistRolesRepository.findByReleaseResourceId(rr.getId());
		for (int i = 0; i < displayArtistName.size(); i++) {
			if (i <= artistRole.size()) {
				String artist = artistRole.get(i);
				if (!Filter.isNullOrEmpty(artist) && artist.equalsIgnoreCase("MainArtist") || artist.equalsIgnoreCase("FeaturedArtist")) {
					DisplayArtist displayArtist = new DisplayArtist();
					displayArtist.setName((String) displayArtistName.get(i)[0]);
					String imageCode = (String) displayArtistName.get(i)[1];
					if (imageCode != null && !imageCode.isEmpty()) {
						displayArtist.setDisplayArtistImageCode(imageCode);
						ReleaseResources releaseResource = releaseResourcesRepository.findByResourceCode(imageCode);
						if (Objects.nonNull(releaseResource) && !CollectionUtils.isEmpty(releaseResource.getImages())) {
							Iterator<Images> images = releaseResource.getImages().iterator();
							if (images.hasNext())
								rl.setTechnicalImageFileDetails(this.generateImageFileDetails(images.next().getTechnicalImageFileDetails()));
						}
					}
					displayArtist.setRole(artist);
					displayArtistList.add(displayArtist);
				}
			}
		}
		if (Objects.isNull(displayArtistList) && displayArtistList.isEmpty())
			message.add("Display Artist Not Found");
		return displayArtistList;
	}

	public List<DisplayArtist> generateDisplayArtistNew(ReleaseResources rr, ResourceList rl) {
		List<DisplayArtist> displayArtistList = new ArrayList<>();
		List<Object[]> displayArtistName = displayArtistRepository.findByReleaseResourceId(rr.getId());
		List<String> artistRole = artistRolesRepository.findByReleaseResourceId(rr.getId());
		for (int i = 0; i < displayArtistName.size(); i++) {
			if (i <= artistRole.size()) {
				String artist = artistRole.get(i);
				if (!Filter.isNullOrEmpty(artist) && artist.equalsIgnoreCase("MainArtist") || artist.equalsIgnoreCase("FeaturedArtist")) {
					DisplayArtist displayArtist = new DisplayArtist();
					displayArtist.setName((String) displayArtistName.get(i)[0]);
					String imageCode = (String) displayArtistName.get(i)[1];
					if (imageCode != null && !imageCode.isEmpty()) {
						displayArtist.setDisplayArtistImageCode(imageCode);
						ReleaseResources releaseResource = releaseResourcesRepository.findByResourceCode(imageCode);
						if (Objects.nonNull(releaseResource) && !CollectionUtils.isEmpty(releaseResource.getImages())) {
							Iterator<Images> images = releaseResource.getImages().iterator();
							if (images.hasNext())
								rl.setTechnicalImageFileDetails(this.generateImageFileDetails(images.next().getTechnicalImageFileDetails()));
						}
					}
					displayArtist.setRole(artist);
					displayArtistList.add(displayArtist);
				}
			}
		}
		if (Objects.isNull(displayArtistList) && displayArtistList.isEmpty())
			message.add("Display Artist Not Found");
		return displayArtistList;
	}
	
	private List<String> createTag(ReleaseResources rr) {
		List<ReleaseResourceTags> resourceTags = repoFactory.getReleaseResourceTags().findByReleaseResourceId(rr.getId());
		if (!ObjectsUtil.isNullOrEmpty(resourceTags)) {
			List<String> tagList = new ArrayList<>();
			for (ReleaseResourceTags tags : resourceTags) {
				if (Objects.nonNull(tags.getTags()))
					tagList.add(tags.getTags().getTag());
			}
			return tagList;
		}
		return new ArrayList<String>();
	}
	// public Set<String> generateTerritoryCodes(List<Deals> dealsList) {
	// Set<String> territoryCodes = new HashSet<>();
	// for (Deals deals : dealsList) {
	// for (DealTermsTerritoryCodes codes : deals.getDealTermsTerritoryCodes())
	// territoryCodes.add(codes.getTerritoryCodes().getDdex_territory_code());
	// }
	// if (Objects.isNull(territoryCodes) && territoryCodes.isEmpty())
	// message.add("Territory Code Not Found");
	// return territoryCodes;
	// }

	@Autowired
	private ContentPartnersRepository repo;
	@Autowired
	private ReleasesRepository releasesRepository;
	@Autowired
	private DealRepository dealRepository;
	@Autowired
	private DisplayArtistRepository displayArtistRepository;
	@Autowired
	private ArtistRolesRepository artistRolesRepository;
	@Autowired
	private GenresRepository genresRepository;
	@Autowired
	private ReleaseResourcesRepository releaseResourcesRepository;
	@Autowired
	private RepositoryFactory repoFactory;
}
