package com.spice.distribution.service.impl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.spice.distribution.cms.entity.ContentRefreshingTransactionDetails;
import com.spice.distribution.cms.entity.ContentRefreshingTransactionStatuses;
import com.spice.distribution.cms.entity.ContentRefreshingTransactions;
import com.spice.distribution.request.refreshment.Status;
import com.spice.distribution.request.refreshment.TransactionsRequest;
import com.spice.distribution.response.Respond;
import com.spice.distribution.service.Refreshment;
import com.spice.distribution.service.SimplePageable;
import com.spice.distribution.util.Generate;

/**
 * @author ankit
 *
 */
public abstract class AbstractRefreshment implements Refreshment {

	@Override
	public <R> Page<R> getTransactions(Pageable t) {
		throw Generate.uoe();
	}
	
	@Override
	public <R> Page<R> getTransactionsMziiki(Pageable t) {
		throw Generate.uoe();
	}
	
	
	@Override
	public <R> Page<R> getTransactionsByUserId(int id,Pageable t) {
		throw Generate.uoe();
	}	
	

	@Override
	public List<ContentRefreshingTransactionDetails> getTransactionDetails(int id) {
		throw Generate.uoe();
	}

	@Override
	public Respond saveOrUpdate(ContentRefreshingTransactions transaction) {
		throw Generate.uoe();
	}

	@Override
	public Respond changeStatus(Status status) {
		throw Generate.uoe();
	}

	@Override
	public List<ContentRefreshingTransactionStatuses> getStatuses() {
		throw Generate.uoe();
	}

	@Override
	public Respond isTransactionActive(TransactionsRequest request) {
		throw Generate.uoe();
	}

	@Override
	public <T> List<T> search(Class<T> clazz, String text, SimplePageable pageInfo, String... fields) {
		throw Generate.uoe();
	}

}
