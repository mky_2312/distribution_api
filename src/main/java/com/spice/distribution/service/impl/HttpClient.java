/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.spice.distribution.cms.entity.ContentRefreshingTransactionDetails;
import com.spice.distribution.cms.entity.ContentRefreshingTransactions;
import com.spice.distribution.request.refreshment.Refreshed;
import com.spice.distribution.request.refreshment.RefreshmentRequest;
import com.spice.distribution.response.Respond;

/**
 *  The class used for sending HTTP Request.
 * @author ankit
 *
 */
public final class HttpClient extends AbstractHttp {

	/**
	 * Instantiates a new http client with the given url.
	 *
	 * @param uri the uri
	 */
	public HttpClient(final String uri) {
		super(uri);
	}

	/**
	 * Send request.
	 *
	 * @param transaction the transaction to be used for the request
	 * @return true, if successful
	 */
	public boolean sendRequest(ContentRefreshingTransactions transaction) {
		return postRequest(createRequest(transaction));
	}

	/**
	 * Post request. to be send.
	 *
	 * @param request the request
	 * @return true, if successful otherwise false.
	 */
	private boolean postRequest(RefreshmentRequest request) {
		try {
			Respond response = post(request);
			return response.isStatus();
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	/**
	 * Creates the request  payload for sending the request.
	 *
	 * @param transaction the transaction
	 * @return the refreshment request representing the payload.
	 */
	protected RefreshmentRequest createRequest(ContentRefreshingTransactions transaction) {
		List<Refreshed> refreshedList = new ArrayList<>();
		for (ContentRefreshingTransactionDetails details : transaction.getContentRefreshingTransactionDetails()) {
			Refreshed refreshed = doCreate(details);
			refreshed.setContainer_id(transaction.getContainer_id());
			refreshedList.add(refreshed);
		}
		return new RefreshmentRequest(refreshedList);
	}

	/**
	 * Do create actual lazy creation of the request payload.
	 *
	 * @param details the details representing individual request data
	 * @return the refreshed the created payload.
	 */
	protected Refreshed doCreate(ContentRefreshingTransactionDetails details) {
		Refreshed refreshed = new Refreshed();
		refreshed.setItem_resource_id(details.getItem_resource_id());
		refreshed.setItem_seq_id(details.getNewDisplayOrder());
		refreshed.setItem_type_id(details.getItem_type_id());
		return refreshed;
	}

}
