package com.spice.distribution.service.impl;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.spice.distribution.DistributionApplication;
import com.spice.distribution.cms.constant.Constants;
import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.cms.repo.ReleaseResourcesRepository;
import com.spice.distribution.request.MetadataRequest;
import com.spice.distribution.request.MetadataRequestISRC;
import com.spice.distribution.response.Respond;
import com.spice.distribution.response.RespondKaraoke;
import com.spice.distribution.response.Response;
import com.spice.distribution.response.cnema.Cnema;
import com.spice.distribution.response.metadata.MetadataResponse;
import com.spice.distribution.response.metadata.ResourceList;
import com.spice.distribution.response.musicScorer.MusicScorerResponse;
import com.spice.distribution.response.safaricom.Audio;
import com.spice.distribution.response.safaricom.SafariComResponse;
import com.spice.distribution.service.MetadataServiceKaraoke;
import com.spice.distribution.service.internal.JsonMapper;

@Service
public class MetadataServiceKaraokeImpl implements MetadataServiceKaraoke<MetadataRequestISRC, Response> {

	@Autowired
	private ReleaseResourcesRepository repo;
	
	@Autowired
	private JsonMapper<Collection<ReleaseResources>, Response> mapper;
	
	
	@Override
	public Response processMetadataByKaraoke(MetadataRequestISRC t) {
		//List<String> list = repo.findByReleaseISRC(t.getIsrc(),t.getArtist_name(),t.getTitle_text());
		List<Object[]> listResouce = null;
		
		if(t.getIsrc()!=null && !t.getIsrc().equals("")) {
			if(( t.getArtist_name()!=null &&  !t.getArtist_name().equals("") ) && (t.getTitle_text()!=null &&  !t.getTitle_text().equals("")) ){
				listResouce = repo.findKaraoke1(t.getIsrc(),t.getArtist_name(),t.getTitle_text());
				
			}else {
				listResouce = repo.findKaraoke2(t.getIsrc());				
			}
			
		}else {
			if( t.getArtist_name()!=null && t.getTitle_text()!=null) {
				listResouce = repo.findKaraoke3(t.getArtist_name(),t.getTitle_text());
			}
		}		
		if (listResouce.size()>0) {			
				return new RespondKaraoke(200,"Success",true, listResouce.get(0)[0].toString());
			}else {
				return new RespondKaraoke(2000,"Error:Resource Code Not Found",false, "Resource Codes Not Found");
			}			
	}
	
	

	@Override
	public Response getKaraokeMetadata(String sort,int size,int page) {
		List<ReleaseResources> releaseResources = null;
		int pageNumber = page;
		int limit = size;
		
		int offset = limit*pageNumber - limit;
		
		if(sort.equalsIgnoreCase("asc"))
		releaseResources = repo.findByKaraokeMetadataASC(limit,offset);	
		releaseResources = repo.findByKaraokeMetadataDESC(limit,offset);	
		
		
		if (!releaseResources.isEmpty())
			return  mapper.getMapperKaraoke().generate((Collection) releaseResources,999);
		else
		return null;
		
	}
	
	@Override
	public Response getMetadataForKeeng(int service_id) {
		List<ReleaseResources> releaseResources = null;		 
		List<String> resourcCode = new ArrayList();
		try {
			 File file = new File(Constants.FILE_PATH_RESOURCE); 			  
			  BufferedReader br = new BufferedReader(new FileReader(file)); 
			  
			  String st; 
			  while ((st = br.readLine()) != null) {
			    System.out.println(st);
			    if(st.length()==10)
			    resourcCode.add(st);
			  } 
			  releaseResources = repo.findByResourceCodeIn(resourcCode); 
			 
			  if (!releaseResources.isEmpty())
					return  mapper.getMapperKeeng().generate((Collection) releaseResources,334);
				else
				return null;
		} catch (IOException e) {
			return null;
			// TODO Auto-generated catch block
			
		}
	}

}
