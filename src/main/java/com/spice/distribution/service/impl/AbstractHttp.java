/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.service.impl;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.spice.distribution.exceptions.HttpException;
import com.spice.distribution.request.Request;
import com.spice.distribution.response.Respond;
import com.spice.distribution.response.Response;
import com.spice.distribution.service.Http;
import com.spice.distribution.util.Generate;
import com.spice.distribution.util.ObjectsUtil;

/**
 * The Class AbstractHttp representing the base class for sending the Http Request.
 *
 * @author ankit
 */
public abstract class AbstractHttp implements Http<Request, Response> {

	/** The uri. */
	private final String uri;

	/**
	 * Instantiates a new abstract http with the given url
	 *
	 * @param uri the uri
	 */
	public AbstractHttp(final String uri) {
		ObjectsUtil.requireNonNull(uri, "Uri not Found!!");
		this.uri = uri;
	}

	/* (non-Javadoc)
	 * @see com.spice.distribution.service.Http#post(com.spice.distribution.request.Request)
	 */
	@Override
	public Respond post(Request t) throws HttpException {
		String s = buildTemplate().postForObject(uri, t, String.class);
		return Generate.deserialize(s, Respond.class);
	}

	/**
	 * Builds the template. for sending the request.
	 *
	 * @return the rest template representing template for sending generic request.
	 */
	private RestTemplate buildTemplate() {
		RestTemplate rt = new RestTemplate();
		mapper(rt);
		return rt;
	}

	// Client does not need to know the details of private implementation.
	private void mapper(RestTemplate rt) {
		mapToJson(rt);
		mapToString(rt);
	}

	private void mapToJson(RestTemplate rt) {
		rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	}

	private void mapToString(RestTemplate rt) {
		rt.getMessageConverters().add(new StringHttpMessageConverter());
	}

}
