package com.spice.distribution.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spice.distribution.cms.entity.SafariComServicesResponse;
import com.spice.distribution.request.MetadataRequest;
import com.spice.distribution.request.MetadataRequestISRC;
import com.spice.distribution.response.Response;
import com.spice.distribution.service.MetadataService;
import com.spice.distribution.util.DBType;
import com.spice.distribution.util.MetadataUtil;

/**
 * @author ankit
 *
 */
@Service
public class MetadataServiceImpl implements MetadataService<MetadataRequest, Response> {

	@Autowired
	MetadataUtil metadataUtil;

	@Override
	public Response processMetadataByResources(MetadataRequest t) {
		return metadataUtil.getMetadata(t.getMetadataSource()).processByResource(t);
	}
	
	

	@Override
	public Response processMetadataByReleases(MetadataRequest t) {
		return metadataUtil.getMetadata(t.getMetadataSource()).processByRelease(t);
	}

	@Override
	public Response processMetadataFeed(int t) {
		return metadataUtil.getMetadata(DBType.CMS.name()).processFeed(t);
	}

}
