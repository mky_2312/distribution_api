package com.spice.distribution.service.impl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.spice.distribution.cms.entity.Services;
import com.spice.distribution.entity.DistributionActions;
import com.spice.distribution.entity.DistributionSources;
import com.spice.distribution.entity.DistributionTransactions;
import com.spice.distribution.entity.DistributionTypes;
import com.spice.distribution.entity.Distributions;
import com.spice.distribution.response.Respond;
import com.spice.distribution.service.DistributionService;
import com.spice.distribution.util.Generate;

/**
 * @author ankit
 *
 */
public abstract class DistributorAdapter implements DistributionService {

	@Override
	public Iterable<DistributionActions> processAction() {
		throw Generate.uoe();
	}

	@Override
	public Iterable<DistributionSources> processSource() {
		throw Generate.uoe();
	}
	

	@Override
	public Iterable<DistributionTypes> processType() {
		throw Generate.uoe();
	}

	@Override
	public Respond processTransactions(DistributionTransactions distributionTransactions) {
		throw Generate.uoe();
	}

	@Override
	public Iterable<Distributions> processAllTransactions() {
		throw Generate.uoe();
	}

	@Override
	public <T> Iterable<T> processTransactionById(int id) {
		throw Generate.uoe();
	}

	@Override
	public Page<Distributions> processTransactionPaging(Pageable pageable) {
		throw Generate.uoe();
	}
	
	@Override
	public Page<Distributions> processTransactionMziikiPaging(Pageable pageable) {
		throw Generate.uoe();
	}

	@Override
	public List<Services> processServices(Integer typeId, Integer categoryId) {
		throw Generate.uoe();
	}

	@Override
	public byte[] processArtists(int id) throws Exception {
		throw Generate.uoe();
	}
	

}
