package com.spice.distribution.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import com.spice.distribution.cms.repo.DisplayArtistRepository;
import com.spice.distribution.entity.DistributionTransactionReleases;
import com.spice.distribution.entity.DistributionTransactionResources;
import com.spice.distribution.entity.DistributionTransactions;
import com.spice.distribution.entity.Distributions;
import com.spice.distribution.entity.Distributions1;
import com.spice.distribution.entity.Status;
import com.spice.distribution.entity.constant.DistributionTransactionStatuses;
import com.spice.distribution.entity.constant.DistributionType;
import com.spice.distribution.response.Respond;
import com.spice.distribution.service.internal.Excel;
import com.spice.distribution.util.Filter;
import com.spice.distribution.util.Generate;
import com.spice.distribution.util.RepositoryFactory;

/**
 * @author ankit
 *
 */
@Service("transaction")
public class DTransaction extends DistributorAdapter {
	@Override
	public Respond processTransactions(DistributionTransactions distributionTransactions) {
		distributionTransactions.setDistribution_transaction_status_id(DistributionTransactionStatuses.Initiated.getStatusId());
		this.save(distributionTransactions);
		if (DistributionType.Resources.getTypeId() == distributionTransactions.getDistribution_type_id())
			this.saveResource(distributionTransactions);
		else if (DistributionType.Releases.getTypeId() == distributionTransactions.getDistribution_type_id())
			this.saveRelease(distributionTransactions);
		else
			return response.getError("Unknown Distribution Type Id");
		this.update(distributionTransactions);
		return response.getSuccess("Transaction Successfully Created");
	}

	public void save(DistributionTransactions t) {
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = distributionTransactionManager.getTransaction(def);
		repositoryFactory.getDistributionTransaction().save(t);
		distributionTransactionManager.commit(status);
	}
	public void saveResource(DistributionTransactions t) {
		Set<DistributionTransactionResources> resources = t.getDistributionTransactionResources();
		Filter.setTransactionId(resources, t.getId());
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = distributionTransactionManager.getTransaction(def);
		repositoryFactory.getDistributionTransactionResource().save(resources);
		distributionTransactionManager.commit(status);
	}

	public void saveRelease(DistributionTransactions t) {
		Set<DistributionTransactionReleases> releases = t.getDistributionTransactionReleases();
		Filter.setTransactionIdForRelease(releases, t.getId());
		TransactionDefinition def = new DefaultTransactionDefinition();
		TransactionStatus status = distributionTransactionManager.getTransaction(def);
		repositoryFactory.getDistributionTransactionRelease().save(releases);
		distributionTransactionManager.commit(status);
	}

	@Override
	public Iterable<Distributions> processAllTransactions() {
		Iterable<Distributions> dis = repositoryFactory.getDistribution().findAllByOrderByIdDesc();
		Iterator<Distributions> dis1 = dis.iterator();
		boolean status = dis1.hasNext();
		if (status)
			return this.processStatus1((List<Distributions>) dis);
		return dis;
	}

	public Page<Distributions> processStatus(Page<Distributions> dis) {
		List<Object[]> status = repositoryFactory.getDistributionTransactionResource().findStatus(Filter.filterTransactionId(dis));
		for (Distributions distributions : dis) {
			Object[] ob = this.doFindStatus(status, distributions.getId());
			if (ob != null) {
				Status stat = distributions.getResourceStatus();
				stat.setPending((BigDecimal) ob[0]);
				stat.setSuccessfull((BigDecimal) ob[1]);
				stat.setFailed((BigDecimal) ob[2]);
				stat.setTotal(stat.getPending().add(stat.getSuccessfull().add(stat.getFailed())));
			}
		}
		return (dis);
	}

	public List<Distributions> processStatus1(List<Distributions> dis) {
		List<Object[]> status = repositoryFactory.getDistributionTransactionResource().findStatus(Filter.filterTransactionId(dis));
		for (Distributions distributions : dis) {
			Object[] ob = this.doFindStatus(status, distributions.getId());
			if (ob != null) {
				Status stat = distributions.getResourceStatus();
				stat.setPending((BigDecimal) ob[0]);
				stat.setSuccessfull((BigDecimal) ob[1]);
				stat.setFailed((BigDecimal) ob[2]);
				stat.setTotal(stat.getPending().add(stat.getSuccessfull().add(stat.getFailed())));
			}
		}
		return (dis);
	}

	private Object[] doFindStatus(List<Object[]> list, int id) {
		for (Object[] ob : list) {
			if ((int) ob[3] == id)
				return ob;
		}
		return null;
	}

	@Override
	public Page<Distributions> processTransactionPaging(Pageable pageable) {
		Page<Distributions> distributions = repositoryFactory.getDistributionPaging().findAll(pageable);
		Iterator<Distributions> dis1 = distributions.iterator();
		if (dis1.hasNext())
			return this.processStatus(distributions);
		return distributions;
	}
	
	@Override
	public Page<Distributions> processTransactionMziikiPaging(Pageable pageable) {
		Page<Distributions> distributions = (Page<Distributions>) repositoryFactory.getDistributionPaging().getByService(pageable,"Mziiki");
		Iterator<Distributions> dis1 = distributions.iterator();
		if (dis1.hasNext())
			return this.processStatus(distributions);
		return distributions;
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public <T> Iterable<T> processTransactionById(int id) {
		Distributions1 distributions1 = repositoryFactory.getDistribution1().findOne(id);
		return (Objects.nonNull(distributions1)) ? (distributions1.getDistribution_type_id().equals(2)) ? (Iterable<T>) distributions1.getDistributionTransactionResources() : (Iterable<T>) distributions1.getDistributionTransactionReleases() : (Iterable<T>) Arrays.asList(new Respond(false, "Transaction Id Not Found"));
	}

	public void update(DistributionTransactions t) {
		t.setDistribution_transaction_status_id(DistributionTransactionStatuses.Submitted.getStatusId());
		this.save(t);
	}

	@Override
	public byte[] processArtists(int id) {
		List<Object[]> artists = displayArtistRepository.findByDistributionId(id);
		Workbook workbook = excelView.buildExcelDocument(artists);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try {
			workbook.write(stream);
		} catch (IOException e) {
			//Client does not need to know
		}
		return stream.toByteArray();

	}

	@Autowired
	private Generate response;

	@Autowired
	@Qualifier("distributionTransactionManager")
	private PlatformTransactionManager distributionTransactionManager;

	@Autowired
	private RepositoryFactory repositoryFactory;

	@Autowired
	private Excel excelView;

	@Autowired
	private DisplayArtistRepository displayArtistRepository;
}
