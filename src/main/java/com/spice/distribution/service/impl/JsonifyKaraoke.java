package com.spice.distribution.service.impl;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import javax.naming.OperationNotSupportedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import com.google.common.base.Joiner;
import com.spice.distribution.cms.constant.Constants;
import com.spice.distribution.cms.constant.ResourceType;
import com.spice.distribution.cms.entity.ContentPartners;
import com.spice.distribution.cms.entity.Images;
import com.spice.distribution.cms.entity.ReleaseResourceTags;
import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.cms.entity.Releases;
import com.spice.distribution.cms.entity.SoundRecordings;
import com.spice.distribution.cms.entity.TechnicalSoundRecordingFileDetails;
import com.spice.distribution.cms.entity.Videos;
import com.spice.distribution.cms.repo.ArtistRolesRepository;
import com.spice.distribution.cms.repo.ContentPartnersRepository;
import com.spice.distribution.cms.repo.DealRepository;
import com.spice.distribution.cms.repo.DisplayArtistRepository;
import com.spice.distribution.cms.repo.GenresRepository;
import com.spice.distribution.cms.repo.ReleaseResourcesRepository;
import com.spice.distribution.cms.repo.ReleasesRepository;
import com.spice.distribution.cms.repo.TechnicalSoundFileDetailRepository;
import com.spice.distribution.exceptions.DistributionException;
import com.spice.distribution.response.musicScorer.DisplayArtist;
import com.spice.distribution.response.musicScorer.DownloadUrls;
import com.spice.distribution.response.musicScorer.Genre;
import com.spice.distribution.response.musicScorer.MusicScorerResponse;
import com.spice.distribution.response.musicScorer.Release;
import com.spice.distribution.response.musicScorer.ReleaseImageUrls;
import com.spice.distribution.response.musicScorer.ResourceFiles;
import com.spice.distribution.response.musicScorer.ResourceList;
import com.spice.distribution.response.musicScorer.StreamingUrls;
import com.spice.distribution.response.urls.AbstractImageUrls;
import com.spice.distribution.response.urls.AbstractSoundAndVideoUrls;
import com.spice.distribution.service.Jsonify;
import com.spice.distribution.service.internal.CommonJson;
import com.spice.distribution.service.internal.MusicScorerUrl;
import com.spice.distribution.util.Filter;
import com.spice.distribution.util.ObjectsUtil;
import com.spice.distribution.util.RepositoryFactory;

@Component("karaokeJson")
@Scope(proxyMode = ScopedProxyMode.INTERFACES, value = "prototype")
public class JsonifyKaraoke extends CommonJson implements Jsonify<Collection<ReleaseResources>, MusicScorerResponse>{
	private static final String SEPARATOR = ",";
	private int serviceId;

	@Override
	public MusicScorerResponse generate(Collection<ReleaseResources> t,int serviceId) {
		Objects.requireNonNull(t, "ReleaseResources Not Found");
		MusicScorerResponse musicScorer = new MusicScorerResponse();
		this.serviceId=serviceId;
		musicScorer.setResourceList(generateResourceList((List<ReleaseResources>) t));
		return musicScorer;
	}
	
	@Override
	public MusicScorerResponse generateKaraoke(Collection<ReleaseResources> t) {
		Objects.requireNonNull(t, "ReleaseResources Not Found");
		MusicScorerResponse musicScorer = new MusicScorerResponse();
		this.serviceId=serviceId;
		musicScorer.setResourceList(generateResourceList((List<ReleaseResources>) t));
		return musicScorer;
	}

	private List<ResourceList> generateResourceList(List<ReleaseResources> list) {
		List<ResourceList> resourceList = new ArrayList<>();
		for (ReleaseResources resources : list)
			resourceList.add(this.generateResource(resources));
		return resourceList;
	}

	private ResourceList generateResource(ReleaseResources rr) {
		ResourceList rl = new ResourceList();
		rl.setResourceCode(rr.getResourceCode());
		this.generateSubResource(rr, rl);
		if (Objects.nonNull(rr.getLanguage()))
			rl.setLanguageCodes(Arrays.asList(rr.getLanguage().getIso639_1()));
		//rl.setResourceTags(createTag(rr));
		//rl.setIsPremium(rr.getIs_premium());
		//rl.setPublicationDate(new SimpleDateFormat("MM/dd/yyyy KK:mm:ss a Z").format(new Date()));
		ContentPartners contentPartners = repo.findOne(rr.getContent_partner_id());
		//rl.setContentPartner(contentPartners.getContent_partner());
		rl.setCountryOfOrigin(this.generateCountryOfOrigin(contentPartners.getCountries()));
		Releases releases = releasesRepository.findOne(rr.getReleaseId());
		Release release = this.generateReleases(rr, releases);
		this.generateTDG(rr, rl, releases.getId());
		rl.setRelease(release);
		rl.setResourceFiles(this.generateResourceFile(rr, release));
		createLog(rl);
		return rl;
	}

	private ResourceFiles generateResourceFile(ReleaseResources rr, Release release) {
		ResourceFiles resourceFiles=new ResourceFiles();
		ReleaseImageUrls releaseImageUrls = new ReleaseImageUrls();
		
		releaseImageUrls.setL("http://cdn.mziiki.com/707/0947/356/08/707094735608127_0.jpg");
		releaseImageUrls.setM("http://cdn.mziiki.com/707/0947/356/08/707094735608127_0.jpg");
		releaseImageUrls.setS("http://cdn.mziiki.com/707/0947/356/08/707094735608127_0.jpg");
		releaseImageUrls.setXs("http://cdn.mziiki.com/707/0947/356/08/707094735608127_0.jpg");
			
		if(ObjectsUtil.isNullOrEmpty(release.getReleaseImageUrls())) {
			resourceFiles.setArtwork("http://cdn.mziiki.com/707/0947/356/08/707094735608127_0.jpg");	
			release.setReleaseImageUrls(releaseImageUrls);		
			
			message.add("Art work Default found");
		}else {
			if(ObjectsUtil.isNullOrEmpty(release.getReleaseImageUrls().getXs())) {
				resourceFiles.setArtwork("http://cdn.mziiki.com/707/0947/356/08/707094735608127_0.jpg");	
				release.setReleaseImageUrls(releaseImageUrls);
				message.add("Art work Default found");
			}else {
				resourceFiles.setArtwork(release.getReleaseImageUrls().getXs());
			}
		}
		
		
//		if(ObjectsUtil.isNullOrEmpty(release)) {
//			resourceFiles.setArtwork("http://cdn.mziiki.com/707/0947/356/08/707094735608127_0.jpg");			
//			message.add("Art work Default found");
//		}else {
//			
//		resourceFiles.setArtwork(release.getReleaseImageUrls().getXs());}
		generateSoundrecodingFile(resourceFiles,rr);
		/*resourceFiles.setKaraoke("http://cdn.mziiki.com/101/3140/396/07/101314039607057_0.mp3");
		resourceFiles.setLyrics("http://cdn.mziiki.com/101/3140/396/07/1013140396_01_030_audkrk.mp3");
		resourceFiles.setOriginalTrack("http://horizon.spiceafrica.com/lyrics/1013140396_01_030_lyrics.docx");*/
		return resourceFiles;
	}

	private void generateSoundrecodingFile(ResourceFiles resourceFiles, ReleaseResources rr) {
		Set<SoundRecordings> recordings=rr.getSoundRecordings();
		boolean isOriginal=false;
		boolean isKaroke=false;
		boolean isLyrics=false;
		if(!ObjectsUtil.isNullOrEmpty(recordings)) {
			Optional<SoundRecordings> recording=recordings.stream().findFirst();
			List<TechnicalSoundRecordingFileDetails> details=technicalSoundFileDetailRepository.findBySoundRecordingIdOrderByDurationDesc(recording.get().getId());
			AbstractSoundAndVideoUrls abstractSoundAndVideoUrls	=new	AbstractSoundAndVideoUrls(null,null,rr.getResourceCode());
			for(TechnicalSoundRecordingFileDetails techFileDetails:details) {
				if(techFileDetails.getTechnical_file_type_id()==Constants.ORIGINAL_SOUNDRECODING_ID) {
					resourceFiles.setOriginalTrack(abstractSoundAndVideoUrls.createSoundRecodingUrl(techFileDetails.getFile_name(), techFileDetails.getFile_path()));
					isOriginal=true;
				}else if (techFileDetails.getTechnical_file_type_id()==Constants.KARAOKE_ID) {
					resourceFiles.setKaraoke(abstractSoundAndVideoUrls.createSoundRecodingUrl(techFileDetails.getFile_name(), techFileDetails.getFile_path()));
					isKaroke=true;
				}else if (techFileDetails.getTechnical_file_type_id()==Constants.LYRICS_ID) {
					resourceFiles.setLyrics(abstractSoundAndVideoUrls.createSoundRecodingUrl(techFileDetails.getFile_name(), techFileDetails.getFile_path()));
					isLyrics=true;	
				}
			}
			
		}
		UpdateMessageMissingFile(isOriginal,isKaroke,isLyrics);
		
	}

	private void UpdateMessageMissingFile(boolean isOriginal, boolean isKaroke, boolean isLyrics) {
		if(!isOriginal) {
			message.add("original file not found");
		} if (!isKaroke) {
			message.add("Karaoke file not found");
		}if (!isLyrics) {
			message.add("Lyrics file not found");
		}
		
	}

	@SuppressWarnings("unused")
	private List<String> createTag(ReleaseResources rr) {
		List<ReleaseResourceTags> resourceTags = repoFactory.getReleaseResourceTags().findByReleaseResourceId(rr.getId());
		if (!ObjectsUtil.isNullOrEmpty(resourceTags)) {
			List<String> tagList = new ArrayList<>();
			for (ReleaseResourceTags tags : resourceTags) {
				if (Objects.nonNull(tags.getTags()))
					tagList.add(tags.getTags().getTag());
			}
			return tagList;
		}
		return null;
	}

	private void createLog(ResourceList rl) {
		if (message.isEmpty()) {
			rl.setStatus(true);
			rl.setMessage("Successfull");
		} else {
			rl.setMessage(Joiner.on(SEPARATOR).join(message));
			message.clear();
		}
	}

	public StreamingUrls generateStreamingUrls(String resourceCode) {
		MusicScorerUrl urls = new MusicScorerUrl(new AbstractSoundAndVideoUrls(resourceCode),serviceId);
		return urls.generateStreamingUrls();
	}

	public DownloadUrls generateDownloadUrls(String resourceCode) {
		MusicScorerUrl urls = new MusicScorerUrl(new AbstractSoundAndVideoUrls(resourceCode),serviceId);
		return urls.generateDownloadUrls();
	}

	public Release generateReleases(ReleaseResources rr, Releases releases) {
		Release release = new Release();
		release.setTitle(releases.getTitle_text());
		release.setUPC(releases.getUpc());
		release.setReleaseType(releases.getRelease_type().getRelease_type());
		List<ReleaseResources> releaseResourcesImageCode = releaseResourcesRepository.findByReleaseId(releases.getId());
		for (ReleaseResources imageCode : releaseResourcesImageCode) {
			if (imageCode.getResource_type_id().equals(ResourceType.Image.getId())) {
				if (ObjectsUtil.isNotNullOrEmpty(imageCode.getResourceCode())) {
					MusicScorerUrl urls = new MusicScorerUrl(new AbstractImageUrls(imageCode.getResourceCode()),serviceId);
					release.setReleaseImageUrls(urls.generateReleaseImageUrls());
				}
			}
		}
		return release;
	}

	private void genrerateSound(ReleaseResources rr, ResourceList rl) {
		rl.setResourceType(ResourceType.SoundRecording.name());
		for (SoundRecordings recordings : rr.getSoundRecordings()) {
			rl.setISRC(recordings.getIsrc());
			rl.setTitle(recordings.getTitle_text());
			rl.setResourceSubType(recordings.getSoundRecordingTypes().getSound_recording_type());
			rl.setDuration(repoFactory.getTechnicalSoundFileRepository().maxDuration(recordings.getId()));
		}
	}

	private void generateImage(ReleaseResources rr, ResourceList rl) {
		rl.setResourceType(ResourceType.Image.name());
		for (Images image : rr.getImages()) {
			rl.setTitle(image.getTitle_text());
			rl.setResourceSubType(image.getImageTypes().getImage_type());
		}
	}

	private void generateVideo(ReleaseResources rr, ResourceList rl) {
		rl.setResourceType(ResourceType.Video.name());
		for (Videos video : rr.getVideos()) {
			rl.setISRC(video.getIsrc());
			rl.setTitle(video.getTitle_text());
			rl.setResourceSubType(video.getVideoTypes().getVideo_type());
			rl.setDuration(repoFactory.getTechnicalVideoFileRepository().maxDuration(video.getId()));
		}
	}

	private void generateText(ReleaseResources rr, ResourceList rl) {
		throw new DistributionException(new OperationNotSupportedException("Text is not Supported in this Version"));
	}

	private void generateSubResource(ReleaseResources rr, ResourceList rl) {
		if ((rr.getResource_type_id().equals(ResourceType.SoundRecording.getId())))
			this.genrerateSound(rr, rl);
		else if ((rr.getResource_type_id().equals(ResourceType.Image.getId())))
			this.generateImage(rr, rl);
		else if ((rr.getResource_type_id().equals(ResourceType.Video.getId()))) {
			this.generateVideo(rr, rl);
		} else if ((rr.getResource_type_id().equals(ResourceType.Text.getId())))
			this.generateText(rr, rl);
	}

	public void generateTDG(ReleaseResources rr, ResourceList resourceList, int id) {
		resourceList.setTerritoryCodes(this.generateTerritoryCodes(dealRepository.findByReleaseId(id)));
		resourceList.setDisplayArtists(this.generateDisplayArtist(rr, resourceList));
		resourceList.setGenres(this.generateGenre(rr, resourceList));
	}

	public List<DisplayArtist> generateDisplayArtist(ReleaseResources rr, ResourceList rl) {
		List<DisplayArtist> displayArtistList = new ArrayList<>();
		List<Object[]> displayArtistName = displayArtistRepository.findByReleaseResourceId(rr.getId());
		List<String> artistRole = artistRolesRepository.findByReleaseResourceId(rr.getId());
		for (int i = 0; i < displayArtistName.size(); i++) {
			if (i <= artistRole.size()) {
				String artist = artistRole.get(i);
				if (!Filter.isNullOrEmpty(artist) && artist.equalsIgnoreCase("MainArtist") || artist.equalsIgnoreCase("FeaturedArtist")) {
					DisplayArtist displayArtist = new DisplayArtist();
					displayArtist.setName((String) displayArtistName.get(i)[0]);
					String imageCode = (String) displayArtistName.get(i)[1];
					if (ObjectsUtil.isNotNullOrEmpty(imageCode)) {
						MusicScorerUrl urls = new MusicScorerUrl(new AbstractImageUrls(imageCode),0);
						displayArtist.setArtistImageUrls(urls.generateArtistImageUrls());
					}
					displayArtist.setRole(artist);
					displayArtistList.add(displayArtist);
				}
			}
		}
		if (Objects.isNull(displayArtistList) && displayArtistList.isEmpty())
			message.add("Display Artist Not Found");
		return displayArtistList;
	}

	public List<Genre> generateGenre(ReleaseResources rr, ResourceList rl) {
		List<Genre> genreList = new ArrayList<>();
		List<Object[]> genres = genresRepository.findByReleaseResourceId(rr.getId());
		for (Object[] obj : genres) {
			Genre genre = new Genre();
			genre.setGenre((String) obj[0]);
			String imageCode = (String) obj[1];
			if (ObjectsUtil.isNotNullOrEmpty(imageCode)) {
				MusicScorerUrl urls = new MusicScorerUrl(new AbstractImageUrls(imageCode),0);
				genre.setGenreImageUrls(urls.generateGenreImageUrls());
			}
			genreList.add(genre);
		}
		if (Objects.isNull(genreList) && genreList.isEmpty())
			message.add("Genre Not Found");
		return genreList;
	}

	@Autowired
	private GenresRepository genresRepository;
	@Autowired
	private ArtistRolesRepository artistRolesRepository;
	@Autowired
	private DisplayArtistRepository displayArtistRepository;
	@Autowired
	private ContentPartnersRepository repo;
	@Autowired
	private ReleasesRepository releasesRepository;
	@Autowired
	private DealRepository dealRepository;
	@Autowired
	private ReleaseResourcesRepository releaseResourcesRepository;
	@Autowired
	private RepositoryFactory repoFactory;
	@Autowired
	TechnicalSoundFileDetailRepository  technicalSoundFileDetailRepository;
}
