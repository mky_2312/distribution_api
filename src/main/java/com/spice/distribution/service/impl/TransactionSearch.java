/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.spice.distribution.service.SimplePageable;

/**
 *  TransactionSearch.
 *
 * @author ankit
 */
@Repository
@Transactional
@Service("search")
public class TransactionSearch extends AbstractRefreshment {

	/** The entity manager. */
	@Autowired
	@Qualifier("cmsEntityManager")
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see com.spice.distribution.service.impl.AbstractRefreshment#search(java.lang.Class, java.lang.String, java.lang.String[])
	 */

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> search(Class<T> clazz, String text, SimplePageable pageInfo, String... fields) {
		FullTextEntityManager fullTextEntityManager = org.hibernate.search.jpa.Search.getFullTextEntityManager(entityManager);
		QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(clazz).get();
		org.apache.lucene.search.Query query = queryBuilder.keyword().fuzzy().onFields(fields).matching(text).createQuery();
		org.hibernate.search.jpa.FullTextQuery jpaQuery = fullTextEntityManager.createFullTextQuery(query, clazz);
		pageInfo.setTotalElements(jpaQuery.getResultSize());
		jpaQuery.setFirstResult(pageInfo.getFirstResult());
		jpaQuery.setMaxResults(pageInfo.getPageSize());
		return jpaQuery.getResultList();
	}


	
	

}
