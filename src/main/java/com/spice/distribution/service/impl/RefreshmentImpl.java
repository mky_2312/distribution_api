/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.spice.distribution.cms.entity.ContentRefreshingTransactionDetails;
import com.spice.distribution.cms.entity.ContentRefreshingTransactionStatuses;
import com.spice.distribution.cms.entity.ContentRefreshingTransactions;
import com.spice.distribution.request.refreshment.Status;
import com.spice.distribution.request.refreshment.TransactionsRequest;
import com.spice.distribution.response.Respond;
import com.spice.distribution.service.SimplePageable;
import com.spice.distribution.util.Generate;
import com.spice.distribution.util.ObjectsUtil;
import com.spice.distribution.util.RepositoryFactory;

/**
 * The Class RefreshmentImpl implementation for Refreshment.
 *
 * @author ankit
 */
@Service("refreshmentImpl")
public class RefreshmentImpl extends AbstractRefreshment {

	/* (non-Javadoc)
	 * @see com.spice.distribution.service.Refreshment#getTransactions(org.springframework.data.domain.Pageable)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<ContentRefreshingTransactions> getTransactions(Pageable pageable) {
		return factory.getContentRefreshingTransactionRepository().findAll(pageable);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<ContentRefreshingTransactions> getTransactionsMziiki(Pageable pageable) {
		int offset = pageable.getPageSize()*pageable.getPageNumber();
		int size =pageable.getPageSize();
		List<ContentRefreshingTransactions> tt= factory.getContentRefreshingTransactionRepository().getRefreshingTransactionsByServiceName("Mziiki",offset,size);
	
		return  new PageImpl<>(tt);
	}
	
	@Override
	public Page<ContentRefreshingTransactions> getTransactionsByUserId(int user_id,Pageable pageable) {
		
		int offset = pageable.getPageSize()*pageable.getPageNumber();
		int size =pageable.getPageSize();
		List<ContentRefreshingTransactions> tt= factory.getContentRefreshingTransactionRepository().getRefreshingTransactionsByUserId(user_id,offset,size);
	
		return  new PageImpl<>(tt);
	}
	

	/* (non-Javadoc)
	 * @see com.spice.distribution.service.Refreshment#getTransactionDetails(int)
	 */
	@Override
	public List<ContentRefreshingTransactionDetails> getTransactionDetails(int id) {
		return factory.getContentRefreshingTransactionDetailsRepository().findByContentRefreshingTransactionIdOrderByNewDisplayOrderAsc(id);
	}

	/* (non-Javadoc)
	 * @see com.spice.distribution.service.Refreshment#createTransaction(com.spice.distribution.cms.entity.ContentRefreshingTransactions)
	 */
	@Override
	public Respond saveOrUpdate(ContentRefreshingTransactions transaction) {
		Objects.requireNonNull(transaction.getContentRefreshingTransactionDetails(), "ContentRefreshingTransactionDetails Not Found");
		transaction = factory.getContentRefreshingTransactionRepository().save(transaction);
		return response.getSaveSuccess(transaction.getId());
	}

	/* (non-Javadoc)
	 * @see com.spice.distribution.service.Refreshment#changeStatus(com.spice.distribution.request.refreshment.Status)
	 */
	@Override
	public Respond changeStatus(Status status) {
		factory.getContentRefreshingTransactionRepository().setStatus(status.getTransactionId(), status.getOldStatusId(), status.getNewStatusId());
		return response.getSuccess();
	}

	/* (non-Javadoc)
	 * @see com.spice.distribution.service.Refreshment#getStatuses()
	 */
	@Override
	public List<ContentRefreshingTransactionStatuses> getStatuses() {
		return factory.getContentRefreshingTransactionStatusesRepository().findAll();
	}

	/* (non-Javadoc)
	 * @see com.spice.distribution.service.Refreshment#isTransactionActive(com.spice.distribution.request.refreshment.TransactionsRequest)
	 */
	@Override
	public Respond isTransactionActive(TransactionsRequest request) {
		List<Object[]> ids = factory.getContentRefreshingTransactionRepository().isTransactionValid(request.getCountry_id(), request.getCountry_name(), request.getService_id(), request.getService_name(), request.getContainer_id(), request.getContainer_name(), Arrays.asList(1, 2));
		if (ObjectsUtil.isNullOrEmpty(ids))
			return response.getError("Transaction is currently not active");
		return response.getSuccess("Transaction is Pending for Approval");
	}
	
	
	



	/** The response. */
	@Autowired
	private Generate response;
	/** The transactions repository. */
	@Autowired
	private RepositoryFactory factory;

}
