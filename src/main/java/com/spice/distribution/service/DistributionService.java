package com.spice.distribution.service;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.spice.distribution.cms.entity.Services;
import com.spice.distribution.entity.DistributionActions;
import com.spice.distribution.entity.DistributionSources;
import com.spice.distribution.entity.DistributionTransactions;
import com.spice.distribution.entity.DistributionTypes;
import com.spice.distribution.entity.Distributions;
import com.spice.distribution.response.Respond;

/**
 * @author ankit
 *
 */
public interface DistributionService {
	@Cacheable("action")
	Iterable<DistributionActions> processAction();

	@Cacheable("source")
	Iterable<DistributionSources> processSource();

	@Cacheable("type")
	Iterable<DistributionTypes> processType();

	// @CacheEvict(value = "distributions", allEntries = true, beforeInvocation =
	// true)
	Respond processTransactions(DistributionTransactions distributionTransactions);

	// @Cacheable(value = "distributions")
	Iterable<Distributions> processAllTransactions();

	@Cacheable(value = "id", sync = true, key = "#id")
	<T> Iterable<T> processTransactionById(int id);

	Page<Distributions> processTransactionPaging(Pageable pageable);
	
	Page<Distributions> processTransactionMziikiPaging(Pageable pageable);
	
	List<Services> processServices(Integer typeId, Integer categoryId);

	byte[] processArtists(int id) throws Exception;

}
