package com.spice.distribution.repo;

import org.springframework.data.repository.CrudRepository;
import com.spice.distribution.entity.DistributionTransactions;

/**
 * @author ankit
 *
 */
public interface DistributionTransactionRepository extends CrudRepository<DistributionTransactions, Integer> {

}
