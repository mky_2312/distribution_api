package com.spice.distribution.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.entity.Distributions;

/**
 * @author ankit
 *
 */
public interface DistributionRepository extends CrudRepository<Distributions, Integer> {
	List<Distributions> findAllByOrderByIdDesc();
}
