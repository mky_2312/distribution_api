package com.spice.distribution.repo;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.spice.distribution.entity.Distributions;

/**
 * @author ankit
 *
 */
public interface DistributionPagingRepository extends PagingAndSortingRepository<Distributions, Integer> {
	
	Page<Distributions> getByService(Pageable pageable, String service);
	
	
}
