package com.spice.distribution.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "DistributionTransactionResourceStatuses")
public class DistributionTransactionResourceStatuses {
	@Id
	private int id;
	private String distribution_transaction_resource_status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDistribution_transaction_resource_status() {
		return distribution_transaction_resource_status;
	}

	public void setDistribution_transaction_resource_status(String distribution_transaction_resource_status) {
		this.distribution_transaction_resource_status = distribution_transaction_resource_status;
	}

}
