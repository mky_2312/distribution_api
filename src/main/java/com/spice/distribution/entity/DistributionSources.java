package com.spice.distribution.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author ankit
 *
 */
@Entity
public class DistributionSources {

	@Id
	private int id;
	private String distribution_source;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDistribution_source() {
		return distribution_source;
	}

	public void setDistribution_source(String distribution_source) {
		this.distribution_source = distribution_source;
	}

	@Override
	public String toString() {
		return "DistributionSources [id=" + id + ", distribution_source=" + distribution_source + "]";
	}

}
