package com.spice.distribution.entity.constant;

/**
 * @author ankit
 *
 */
public enum DistributionTransactionStatuses {

	Initiated(1), Submitted(2), In_Progress(3), Errors(4), Processed(5);

	private int value;

	private DistributionTransactionStatuses(int value) {
		this.value = value;
	}

	public int getStatusId() {
		return value;
	}

}
