package com.spice.distribution.entity.constant;

/**
 * @author ankit
 *
 */
public enum DistributionType {
	Releases(1), Resources(2);
	private int value;

	private DistributionType(int value) {
		this.value = value;
	}

	public int getTypeId() {
		return value;
	}
}
