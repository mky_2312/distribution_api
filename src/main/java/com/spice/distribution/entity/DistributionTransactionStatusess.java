package com.spice.distribution.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "DistributionTransactionStatuses")
public class DistributionTransactionStatusess {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String distribution_transaction_status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDistribution_transaction_status() {
		return distribution_transaction_status;
	}

	public void setDistribution_transaction_status(String distribution_transaction_status) {
		this.distribution_transaction_status = distribution_transaction_status;
	}

}
