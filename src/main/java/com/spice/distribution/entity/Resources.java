package com.spice.distribution.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "DistributionTransactionResources")
public class Resources {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Integer distribution_transaction_id;
	private String resource_code;

	public Integer getDistribution_transaction_id() {
		return distribution_transaction_id;
	}

	public void setDistribution_transaction_id(Integer distribution_transaction_id) {
		this.distribution_transaction_id = distribution_transaction_id;
	}

	/**
	 * @param id
	 * @param resource_code
	 */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getResource_code() {
		return resource_code;
	}

	public void setResource_code(String resource_code) {
		this.resource_code = resource_code;
	}

}
