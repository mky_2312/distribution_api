package com.spice.distribution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author ankit
 *
 */
@Entity
public class DistributionTransactionReleases {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(insertable = false, updatable = false)
	private Integer distribution_transaction_id;
	@NotNull(message = " Null Release Id Found")
	private Integer release_id;

	/**
	 * @param release_id
	 */

	public DistributionTransactionReleases(Integer release_id) {
		super();
		this.release_id = release_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getDistribution_transaction_id() {
		return distribution_transaction_id;
	}

	public void setDistribution_transaction_id(Integer distribution_transaction_id) {
		this.distribution_transaction_id = distribution_transaction_id;
	}

	public Integer getRelease_id() {
		return release_id;
	}

	public void setRelease_id(Integer release_id) {
		this.release_id = release_id;
	}

	@Override
	public String toString() {
		return "DistributionTransactionReleases [id=" + id + ", distribution_transaction_id=" + distribution_transaction_id + ", release_id=" + release_id + "]";
	}

}
