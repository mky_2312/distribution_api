/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Configuration Indicates that a class declares one or more {@link Bean @Bean} methods and
 * may be processed by the Spring container to generate bean definitions and
 * service requests for those beans at runtime.
 * @EnableTransactionManagement  Enables Spring's annotation-driven transaction management capability, similar to
 * the support found in Spring's {@code <tx:*>} XML namespace.
 * @EnableJpaRepositories  Annotation to enable JPA repositories. Will scan the package of the annotated configuration class for Spring Data
 * repositories by default.
 * 
 * @author ankit
 * @see src/main/resources/application.properties#cms
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.spice.distribution", entityManagerFactoryRef = "cmsEntityManager", transactionManagerRef = "cmsTransactionManager")
public class CMSConfiguration {

	/** Contains all the Runtime details found in Properties*/
	@Autowired
	private Environment environment;

	/**
	 * Represents CMS Datasource defined in properties.
	 *
	 * @return the data source
	 */
	@Bean(name = "cmsDataSource")
	@Primary
	public DataSource cmsDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("cms.db.driver"));
		dataSource.setUrl(environment.getRequiredProperty("cms.db.url"));
		dataSource.setUsername(environment.getRequiredProperty("cms.db.username"));
		dataSource.setPassword(environment.getRequiredProperty("cms.db.password"));
		return dataSource;
	}

	/**
	 *Represents ConnectionPool with the above specified Datasource.
	 *
	 * @return the local container entity manager factory bean
	 */
	@Bean(name = "cmsEntityManager")
	@Primary
	public LocalContainerEntityManagerFactoryBean cmsEntityManager() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(this.cmsDataSource());
		em.setPackagesToScan(new String[] { environment.getRequiredProperty("entitymanager.packages.to.scan") });
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(this.hibernateProperties());
		return em;
	}

	/**
	 * Represents the TransactionManagement in the above specified ConnectionPool.
	 *
	 * @return the platform transaction manager
	 */
	@Bean(name = "cmsTransactionManager")
	@Primary
	public PlatformTransactionManager cmsTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(this.cmsEntityManager().getObject());
		return transactionManager;
	}

	/**
	 *	Represents SessionFactory to be used by Hibernate internally for managing Sessions with the Datasource.
	 *
	 * @return the local session factory bean
	 */
	@Bean(name = "cmsSessionFactory")
	@Primary
	public LocalSessionFactoryBean cmsSessionFactory() {
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(this.cmsDataSource());
		sessionFactoryBean.setPackagesToScan(environment.getRequiredProperty("entitymanager.packages.to.scan"));
		sessionFactoryBean.setHibernateProperties(this.hibernateProperties());
		return sessionFactoryBean;
	}

	/**
	 * Hibernate properties mainly containing all the properties related to Datasource.
	 *
	 * @return the properties
	 */
	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		properties.put("entitymanager.packages.to.scan", environment.getRequiredProperty("entitymanager.packages.to.scan"));
		properties.put("spring.jpa.properties.hibernate.search.default.directory_provider", environment.getRequiredProperty("spring.jpa.properties.hibernate.search.default.directory_provider"));
		properties.put("spring.jpa.properties.hibernate.search.default.indexBase", environment.getRequiredProperty("spring.jpa.properties.hibernate.search.default.indexBase"));
		properties.put("connection.release_mode", environment.getRequiredProperty("connection.release_mode"));
		properties.put("current_session_context_class", "thread");
		return properties;
	}
}
