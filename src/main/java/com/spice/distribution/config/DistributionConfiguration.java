package com.spice.distribution.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * @author ankit
 *
 */
@Configuration
public class DistributionConfiguration {

	@Autowired
	private Environment environment;

	@Bean(name = "distributionDataSource")
	public DataSource distributionDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("distribution.db.driver"));
		dataSource.setUrl(environment.getRequiredProperty("distribution.db.url"));
		dataSource.setUsername(environment.getRequiredProperty("distribution.db.username"));
		dataSource.setPassword(environment.getRequiredProperty("distribution.db.password"));
		return dataSource;
	}

	@Bean(name = "distributionEntityManager")
	public LocalContainerEntityManagerFactoryBean distributionEntityManager() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(this.distributionDataSource());
		em.setPackagesToScan(new String[] { environment.getRequiredProperty("entitymanager.packages.to.scan") });
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(this.hibernateProperties());
		return em;
	}

	@Bean(name = "distributionTransactionManager")
	public PlatformTransactionManager distributionTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(this.distributionEntityManager().getObject());
		return transactionManager;
	}

	@Bean(name = "distributionSessionFactory")
	public LocalSessionFactoryBean distributionSessionFactory() {
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(this.distributionDataSource());
		sessionFactoryBean.setPackagesToScan(environment.getRequiredProperty("entitymanager.packages.to.scan"));
		sessionFactoryBean.setHibernateProperties(this.hibernateProperties());
		return sessionFactoryBean;
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		properties.put("entitymanager.packages.to.scan", environment.getRequiredProperty("entitymanager.packages.to.scan"));
		properties.put("connection.release_mode", environment.getRequiredProperty("connection.release_mode"));
		properties.put("spring.cache.ehcache.config", environment.getRequiredProperty("spring.cache.ehcache.config"));
		properties.put("hibernate.jdbc.batch_size", 100);
		properties.put("hibernate.jdbc.time_zone", "EST");
		return properties;
	}

}
