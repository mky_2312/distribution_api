/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spice.distribution.DistributionApplication;
import com.spice.distribution.cms.entity.ContentRefreshingTransactionDetails;
import com.spice.distribution.cms.entity.ContentRefreshingTransactionStatuses;
import com.spice.distribution.cms.entity.ContentRefreshingTransactions;
import com.spice.distribution.request.refreshment.Status;
import com.spice.distribution.request.refreshment.TransactionsRequest;
import com.spice.distribution.response.Respond;
import com.spice.distribution.response.Response;
import com.spice.distribution.service.Refreshment;
import com.spice.distribution.service.SimplePageable;
import com.spice.distribution.util.Generate;
import com.spice.distribution.util.PageInfo;

/**
 * A RestController for processing all the request handling for refreshment.
 * @author ankit
 *
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/refreshment")
public class ContentRefreshmentController {
	@Autowired
	@Qualifier("search")
	private Refreshment search;
	/** The response. */
	@Autowired
	private Generate response;

	/** The refreshment. injection. */
	@Autowired
	@Qualifier("refreshmentImpl")
	com.spice.distribution.service.Refreshment refreshment;
	
	
	/**
	 * Gets the transaction.
	 *
	 * @param pageable the pageable
	 * @return the transaction
	 */
	// localhost:8081/refreshment/getTransactions?page=0&size=3&sort=id,desc
	@GetMapping(value = "/getTransactions", produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<ContentRefreshingTransactions> getTransactions(Pageable pageable) {
		DistributionApplication.LOGGER.debug("Request For getTransactions");
		return refreshment.getTransactions(pageable);
	}
	
	@GetMapping(value = "/getTransactions/mziiki/", produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<ContentRefreshingTransactions> getTransactionsMziiki(Pageable pageable) {
		DistributionApplication.LOGGER.debug("Request For getTransactions Mziiki ");
		return refreshment.getTransactionsMziiki(pageable);
	}
/*	
	@GetMapping(value = "/getTransaction/",produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<ContentRefreshingTransactions> getTransaction(@RequestParam("user_id") Integer user_id, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {
		DistributionApplication.LOGGER.debug("================= Request For getTransactions ============================="+user_id);
		return refreshment.getTransactionsByUserId(user_id,page,size);
	}	
*/
	@GetMapping(value = "/getTransactions/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<ContentRefreshingTransactions> getTransaction(@PathVariable("id") int id,Pageable pageable) {
		DistributionApplication.LOGGER.debug("================= Request For getTransactions ============================="+id);
		return refreshment.getTransactionsByUserId(id,pageable);
	}		
	
	/**
	 * Gets the transaction details with the given param.
	 *
	 * @param id the id based on which details has to be found.
	 * @return the list representing ContentRefreshingTransactionDetails.
	 */
	@GetMapping(value = "/getTransactionDetails/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ContentRefreshingTransactionDetails> getTransactionDetails(@PathVariable("id") int id) {
		DistributionApplication.LOGGER.debug("Request For getTransactionDetails");
		return refreshment.getTransactionDetails(id);
	}

	/**
	 * Creates the  new transaction.
	 *
	 * @param e the representing ContentRefreshingTransactions to be added.
	 * @param result the result for validation.
	 * @return the respond representing  status of transaction.
	 */
	@PostMapping(value = "/saveOrUpdate", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response saveOrUpdateTansaction(@RequestBody @Valid ContentRefreshingTransactions e, BindingResult result) {
		DistributionApplication.LOGGER.debug("Request For saveOrUpdate");
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For saveOrUpdate: "+result.getFieldError().getDefaultMessage());
			return response.getError(result.getFieldError().getDefaultMessage());
		}
		return refreshment.saveOrUpdate(e);
	}

	/**
	 * Change status.
	 *
	 * @param status the status contains value to be used.
	 * @param result the result if any error in validation.
	 * @return the response representing status.
	 */
	@PostMapping(value = "/status", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response changeStatus(@Valid Status status, BindingResult result) {
		DistributionApplication.LOGGER.debug("Request For status");
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For status: "+result.getFieldError().getDefaultMessage());
			return response.getError(result.getFieldError().getDefaultMessage());
		}
		return refreshment.changeStatus(status);
	}

	/**
	 * Gets the statuses.
	 *
	 * @return the list representing ContentRefreshingTransactionStatuses.
	 */
	@GetMapping(value = "/getStatuses", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ContentRefreshingTransactionStatuses> getStatuses() {
		DistributionApplication.LOGGER.debug("Request For getStatuses");
		return refreshment.getStatuses();
	}

	/**
	 * Checks if is transaction active or not with the given param.
	 *
	 * @param request the request representing param for verification.
	 * @param result the result if contains any error in validation.
	 * @return the respond representing status of the transaction.nt time
	 */
	@PostMapping(value = "/isTransactionActive", produces = MediaType.APPLICATION_JSON_VALUE)
	public Respond isTransactionActive(@Valid TransactionsRequest request, BindingResult result) {
		DistributionApplication.LOGGER.debug("Request For isTransactionActive");
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For isTransactionActive: "+result.getFieldError().getDefaultMessage());
			
			return response.getError(result.getFieldError().getDefaultMessage());
		}
		return refreshment.isTransactionActive(request);
	}

	/**
	 * Search particular text in the whole transactions.
	 *  Support is also provided with the pagination to handle huge amount of search results
	 *
	 * @param text {@code String} to be searched
	 * @param pageable (@code Pageable) contains info for the pagination.
	 * @return   the list representing ContentRefreshingTransactions with pagination details.
	 */
	//	/search/{text}?page=0&size=3&sort=id,desc
	@GetMapping(value = "/search/{text}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<ContentRefreshingTransactions> search(@PathVariable String text, Pageable pageable) {
		DistributionApplication.LOGGER.debug("Request For search");
		SimplePageable pageInfo = getInfo(pageable);
		return convertToPage(search.search(ContentRefreshingTransactions.class, text, pageInfo, getFields()), pageable, pageInfo);
	}

	//client does not need to know
	private String[] getFields() {
		return new String[] { "service_name", "country_name", "container_name", "contentRefreshingTransactionStatuses.content_refreshing_transaction_status" };
	}

	//client does not need to know
	private SimplePageable getInfo(Pageable pageable) {
		return new PageInfo(pageable.getPageNumber(), pageable.getPageSize());
	}

	//client does not need to know
	private Page<ContentRefreshingTransactions> convertToPage(List<ContentRefreshingTransactions> list, Pageable pageable, SimplePageable pageInfo) {
		Pageable page = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());
		return new PageImpl<ContentRefreshingTransactions>(list, page, pageInfo.getTotalElements());
	}


}
