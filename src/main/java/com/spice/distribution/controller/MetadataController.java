/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.controller;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.QueryParam;

import org.apache.xalan.lib.sql.QueryParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spice.distribution.DistributionApplication;
import com.spice.distribution.cms.entity.ReleaseResources;
import com.spice.distribution.entity.Distributions;
import com.spice.distribution.request.MetadataRequest;
import com.spice.distribution.request.MetadataRequestISRC;
import com.spice.distribution.response.Response;
import com.spice.distribution.service.MetadataService;
import com.spice.distribution.service.MetadataServiceKaraoke;
import com.spice.distribution.util.Generate;

/**
 *  A RestController for processing all the request handling for metadata in Horizon..
 *
 * @author ankit
 * @see com.spice.distribution.request.MetadataRequest
 */

@RestController
@RequestMapping(value = "/metadata")
@CrossOrigin
public class MetadataController {

	/**
	 * Gets all the metadata based on list of resourceCodes .
	 *
	 * @param request representing MetadataRequest.
	 * @param   result  representing any validation error in processing request.
	 * @return the metadata for resources
	 *  @see com.spice.distribution.request.MetadataRequest
	 */
	@RequestMapping(value = "/getMetadataForResources", method = { RequestMethod.GET, RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response getMetadataForResources(@Valid MetadataRequest request, BindingResult result) {
		DistributionApplication.LOGGER.debug("Request For getMetadataForResources");
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For getMetadataForResources: "+result.getFieldError().getDefaultMessage());
			return response.getError(result.getFieldError().getDefaultMessage());
		}
		return service.processMetadataByResources(request);
	}
	
	
	//Servie _id = 334
	@RequestMapping(value = "/getMetadataForKeeng", method = { RequestMethod.GET, RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response getMetadataForKeeng(@RequestParam("service_id") int service_id) {
		DistributionApplication.LOGGER.debug("Request For getMetadataForService");
		
		return serviceKaraoke.getMetadataForKeeng(service_id);
	}
	
	
	
	@RequestMapping(value = "/getMetadataForKaraoke", method = { RequestMethod.GET, RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response getMetadataForResourcesByISRC(@Valid MetadataRequestISRC request, BindingResult result) {
		DistributionApplication.LOGGER.debug(" : Request For getMetadataForKaraoke : ");		
		DistributionApplication.LOGGER.info("Isrc : " + request.getIsrc() +" Artist name  : " + request.getArtist_name() + " Title Text : " + request.getTitle_text());
	
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For getMetadataForResources: "+result.getFieldError().getDefaultMessage());
			return response.getError(result.getFieldError().getDefaultMessage());
		}
		return serviceKaraoke.processMetadataByKaraoke(request);
		
	}
	
	@RequestMapping(value = "/getKaraokeMetadata", method = { RequestMethod.GET, RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response getKaraokeMetadata(@RequestParam("sort") String sort,@RequestParam("size") int size,@RequestParam("page") int page) {
		DistributionApplication.LOGGER.debug(" : Request For getMetadataForKaraoke : ");		
		DistributionApplication.LOGGER.info("Sort : " + sort +" Page Size  : " + size + " Page Number : " +page);
		return (Response) serviceKaraoke.getKaraokeMetadata(sort,size,page);
		//return null;
	}

	
	/**
	 * Gets all the metadata based on releaseId.
	 *
	 * @param request representing MetadataRequest
	 * @param result  representing any validation error in processing request.
	 * @return the metadata for releases
	 * @see com.spice.distribution.request.MetadataRequest
	 */
	@RequestMapping(value = "/getMetadataForReleases", method = { RequestMethod.GET, RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response getMetadataForReleases(@Valid MetadataRequest request, BindingResult result) {
		DistributionApplication.LOGGER.debug("Request For getMetadataForReleases");
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For getMetadataForReleases: "+result.getFieldError().getDefaultMessage());
			return response.getError(result.getFieldError().getDefaultMessage());
		}
		return service.processMetadataByReleases(request);
	}

	@GetMapping(value = "/getMetadataFeed/{serviceId}", produces = MediaType.APPLICATION_XML_VALUE)
	public Response getMetadataFeed(@PathVariable int serviceId) {
		DistributionApplication.LOGGER.debug("Request For getMetadataFeed");
		return service.processMetadataFeed(serviceId);
	}

	/** The service. */
	@Autowired
	private MetadataService<MetadataRequest, Response> service;
	
	@Autowired
	private MetadataServiceKaraoke<MetadataRequestISRC, Response> serviceKaraoke;

	/** The response. */
	@Autowired
	private Generate response;

}
