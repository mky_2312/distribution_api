/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spice.distribution.DistributionApplication;
import com.spice.distribution.cms.entity.Services;
import com.spice.distribution.entity.DistributionActions;
import com.spice.distribution.entity.DistributionSources;
import com.spice.distribution.entity.DistributionTransactions;
import com.spice.distribution.entity.DistributionTypes;
import com.spice.distribution.entity.Distributions;
import com.spice.distribution.response.Respond;
import com.spice.distribution.service.DistributionService;
import com.spice.distribution.util.Generate;

/**
 *  A RestController for processing all the request handling for distribution.
 *
 * @author ankit
 * 
 * @see com.spice.distribution.cms.entity.DistributionActions
 * @see com.spice.distribution.cms.entity.DistributionSources
 * @see com.spice.distribution.cms.entity.DistributionTypes
 * @see com.spice.distribution.cms.entity.DistributionTransactions
 * @see com.spice.distribution.cms.entity.Distributions
 * @see com.spice.distribution.cms.entity.Services
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/distribution")
public class DistributionController {

	/** The Constant MEDIA_TYPE_EXCEL. */
	private static final String MEDIA_TYPE_EXCEL = "application/vnd.ms-excel";

	/**
	 * Gets all the distribution actions defined in Horizon.
	 *
	 * @return the iterable representing distribution actions
	 * 
	 * @see com.spice.distribution.cms.entity.DistributionActions
	 */
	@RequestMapping(value = "/getDistributionActions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<DistributionActions> getDistributionActions() {
		DistributionApplication.LOGGER.debug("Request For getDistributionActions");
		return action.processAction();
	}

	/**
	 * Gets all the distribution sources used in Horizon.
	 *
	 * @return the iterable representing distribution sources
	 * 
	 * @see com.spice.distribution.cms.entity.DistributionSources
	 */
	@RequestMapping(value = "/getDistributionSources", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<DistributionSources> getDistributionSources() {
		DistributionApplication.LOGGER.debug("Request For getDistributionSources");
		return source.processSource();
	}

	/**
	 * Gets all the distribution types defined in Horizon.
	 *
	 * @return the iterable representing distribution types
	 * 
	 * @see com.spice.distribution.cms.entity.DistributionType
	 */
	@RequestMapping(value = "/getDistributionTypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<DistributionTypes> getDistributionTypes() {
		DistributionApplication.LOGGER.debug("Request For getDistributionTypes");
		return type.processType();
	}

	/**
	 * Creates the new transactions for distribution
	 *
	 * @param distributionTransactions containing all the details regarding transactions.
	 * @param result  representing any validation error in creating transactions.
	 * @return the respond for the requested request  describing status.
	 * 
	 * @see com.spice.distribution.cms.entity.DistributionTransactions
	 */
	@RequestMapping(value = "/createTransaction", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Respond createTransactions(@RequestBody @Valid DistributionTransactions distributionTransactions, BindingResult result) {
		DistributionApplication.LOGGER.debug("Request For createTransaction");
		if (result.hasErrors()) {
			DistributionApplication.LOGGER.error("Request error For createTransaction: "+result.getFieldError().getDefaultMessage());
			return response.getError(result.getFieldError().getDefaultMessage());
		}
		return transaction.processTransactions(distributionTransactions);
	}

	/**
	 * Gets  all the transactions created in distributions without describing its resources.
	 *
	 * @return  the iterable representing all transactions
	 * 
	 * @see com.spice.distribution.cms.entity.Distributions
	 */
	@RequestMapping(value = "/getAllTransactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<Distributions> getAllTransactions() {
		DistributionApplication.LOGGER.debug("Request For getAllTransactions");
		return transaction.processAllTransactions();
	}

	/**
	 * Gets the transactions  by its transaction id including its resources
	 *
	 * @param <T> the generic type for describing transactions.
	 * @param id the id
	 * @return the iterable representing  transaction.
	 */
	@GetMapping(value = "/getByTransactionId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public <T> Iterable<T> getByTransactionId(@PathVariable int id) {
		DistributionApplication.LOGGER.debug("Request For getByTransactionId");
		return transaction.processTransactionById(id);
	}

	/**
	 * Gets all the transaction based on Pagination request.
	 *
	 * @param pageable  representing paging request like pageNo,size of data and sorting based on.
	 * @return the transaction
	 * 
	 * @see com.spice.distribution.cms.entity.Distributions
	 */
	// localhost:8081/distribution/getTransaction?page=0&size=3&sort=id,desc
	@GetMapping(value = "/getTransaction", produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<Distributions> getTransaction(Pageable pageable) {
		DistributionApplication.LOGGER.debug("Request For getTransaction");
		return transaction.processTransactionPaging(pageable);
	}
	
	
	@GetMapping(value = "/getTransaction/mziiki/", produces = MediaType.APPLICATION_JSON_VALUE)
	public Page<Distributions> getTransactionMziiki(Pageable pageable) {
		DistributionApplication.LOGGER.debug("Request For getTransaction For Zikki ");
		return transaction.processTransactionMziikiPaging(pageable);
	}
	
	
	
	/**
	 * Gets all the services based on TypeId and CategoryId.
	 *
	 * @param typeId the type id
	 * @param categoryId the category id
	 * @return the List representing all the  services based on given params. 
	 * 
	 * @see com.spice.distribution.cms.entity.Services
	 */
	// http://54.154.195.75:8081/distribution/getServices/?typeId=1&categoryId=2
	@GetMapping(value = "/getServices/", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Services> getServices(@RequestParam("typeId") Integer typeId, @RequestParam("categoryId") Integer categoryId) {
		DistributionApplication.LOGGER.debug("Request For getServices");
		return type.processServices(typeId, categoryId);
	}

	/**
	 * Download all the artists details based on given param
	 *
	 * @param id  representing distribution Id.
	 * @return the response entity containing excel file
	 * @throws Exception the exception if any error occured.
	 */
	@GetMapping(value = "/download/artists/{id}", produces = MEDIA_TYPE_EXCEL)
	public ResponseEntity<byte[]> download(@PathVariable int id) throws Exception {
		DistributionApplication.LOGGER.debug("Request For download");
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"ArtistsDetails.xlsx\"");
		return new ResponseEntity<>(transaction.processArtists(id), headers, HttpStatus.OK);
	}

	/** The response. */
	@Autowired
	private Generate response;

	/** The action. */
	@Autowired
	@Qualifier("action")
	private DistributionService action;

	/** The source. */
	@Autowired
	@Qualifier("source")
	private DistributionService source;

	/** The type. */
	@Autowired
	@Qualifier("type")
	private DistributionService type;

	/** The transaction. */
	@Autowired
	@Qualifier("transaction")
	private DistributionService transaction;
}
