package com.spice.distribution.util;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.spice.distribution.service.SimplePageable;

/**
 * @author ankit
 *
 */
public final class PageInfo implements SimplePageable {

	private final int pageNumber;

	private final int pageSize;

	private int totalElements;

	public PageInfo(int pageNumber, int pageSize) {
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	@Override
	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	@Override
	public int getFirstResult() {
		return (pageNumber * pageSize);
	}

	@Override
	public int getPageNumber() {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getPageSize() {
		return pageSize;
	}

	@Override
	public int getOffset() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Sort getSort() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Pageable next() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Pageable previousOrFirst() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Pageable first() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasPrevious() {
		throw new UnsupportedOperationException();
	}

}
