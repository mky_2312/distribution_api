/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.util;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.spice.distribution.request.Request;
import com.spice.distribution.response.RefreshmentSaveResponse;
import com.spice.distribution.response.Respond;
import com.spice.distribution.response.Response;

// TODO: Auto-generated Javadoc
/**
 * The Class GenerateResponse.
 *
 * @author ankit
 */
@Component
public class Generate implements Request, Response {

	/**
	 * Gets the success.
	 *
	 * @param message the message
	 * @return the Respond representing success.
	 */
	public Respond getSuccess(String message) {
		return new Respond(true, message);
	}

	/**
	 * Gets the success.
	 *
	 * @return   the Respond representing success.
	 */
	public Respond getSuccess() {
		return new Respond(true, "Success");
	}

	/**
	 * Gets the save success.
	 *
	 * @param id the id
	 * @return  the Respond representing success.
	 */
	public Respond getSaveSuccess(Integer id) {
		return new RefreshmentSaveResponse(true, "Success", id);
	}

	/**
	 * Gets the error.
	 *
	 * @param message the message
	 * @return  the Respond representing error.
	 */
	public Respond getError(String message) {
		return new Respond(false, message);
	}

	/**
	 * Serialize the given object into respecting json.
	 *
	 * @param <T> the generic type
	 * @param t the t
	 * @return the string
	 * @throws Exception the exception
	 */
	public static <T> String serialize(T t) throws Exception {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		return ow.writeValueAsString(t);
	}

	/**
	 * Deserialize the given json into the given class type.
	 *
	 * @param <T> the generic type
	 * @param json the json
	 * @param clazz the clazz
	 * @return the t
	 */
	public static <T> T deserialize(String json, Class<T> clazz) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(json, clazz);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * throws unchecked exception.
	 *
	 * @return the unsupported operation exception
	 */
	public static UnsupportedOperationException uoe() {
		return new UnsupportedOperationException();
	}

}
