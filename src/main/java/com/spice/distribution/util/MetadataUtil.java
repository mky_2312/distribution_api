package com.spice.distribution.util;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.spice.distribution.request.MetadataRequest;
import com.spice.distribution.request.MetadataRequestISRC;
import com.spice.distribution.response.Response;
import com.spice.distribution.service.Metadata;

/**
 * @author ankit
 *
 */
@Component
public class MetadataUtil {

	@Autowired
	@Qualifier("cms")
	Metadata<Response, MetadataRequest> cmsMetadata;
	
	public Metadata<Response, MetadataRequest> getMetadata(String type) {
		if ("cms".equalsIgnoreCase(type))
			return cmsMetadata;
		else
			throw new NoSuchElementException("Invalid metadataSourceType");
	}
	
	
}
