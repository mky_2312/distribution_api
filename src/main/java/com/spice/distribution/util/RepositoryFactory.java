package com.spice.distribution.util;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.spice.distribution.cms.repo.ContentRefreshingTransactionDetailsRepository;
import com.spice.distribution.cms.repo.ContentRefreshingTransactionStatusesRepository;
import com.spice.distribution.cms.repo.ContentRefreshingTransactionsRepository;
import com.spice.distribution.cms.repo.DealRepository;
import com.spice.distribution.cms.repo.DealTermsUsageUseTypesRepository;
import com.spice.distribution.cms.repo.ReleaseResourceTagsRepo;
import com.spice.distribution.cms.repo.TechnicalImageFileDetailsRepository;
import com.spice.distribution.cms.repo.TechnicalSoundFileDetailRepository;
import com.spice.distribution.cms.repo.TechnicalVideoFileDetailRepository;
import com.spice.distribution.cms.repo.UseTypesRepository;
import com.spice.distribution.repo.Distribution1Repository;
import com.spice.distribution.repo.DistributionActionsRepository;
import com.spice.distribution.repo.DistributionPagingRepository;
import com.spice.distribution.repo.DistributionRepository;
import com.spice.distribution.repo.DistributionSourceRepository;
import com.spice.distribution.repo.DistributionTransactionReleasesRepository;
import com.spice.distribution.repo.DistributionTransactionRepository;
import com.spice.distribution.repo.DistributionTransactionResourceRepository;
import com.spice.distribution.repo.DistributionTypeRepository;

/**
 * @author ankit
 *
 */
@Service
public class RepositoryFactory {

	public DealRepository getDeals() {
		return dealsRepository;
	}

	/**
	 * @return the dealTermsUsageUseTypesRepository
	 */
	public DealTermsUsageUseTypesRepository getDealTermsUsageUseTypesRepository() {
		return dealTermsUsageUseTypesRepository;
	}

	/**
	 * @return the useTypesRepository
	 */
	public UseTypesRepository getUseTypesRepository() {
		return useTypesRepository;
	}

	public DistributionActionsRepository getDistributionAction() {
		if (Objects.isNull(distributionActionsRepository))
			synchronized (this) {
				distributionActionsRepository = meta.construct(DistributionActionsRepository.class);
			}
		return distributionActionsRepository;
	}

	public DistributionSourceRepository getDistributionSource() {
		if (Objects.isNull(distributionSourceRepository))
			synchronized (this) {
				distributionSourceRepository = meta.construct(DistributionSourceRepository.class);
			}
		return distributionSourceRepository;
	}

	public DistributionTypeRepository getDistributionType() {
		if (Objects.isNull(distributionTypeRepository))
			synchronized (this) {
				distributionTypeRepository = meta.construct(DistributionTypeRepository.class);
			}
		return distributionTypeRepository;
	}

	public DistributionTransactionRepository getDistributionTransaction() {
		if (Objects.isNull(distributionTransactionRepository))
			synchronized (this) {
				distributionTransactionRepository = meta.construct(DistributionTransactionRepository.class);
			}
		return distributionTransactionRepository;
	}

	public DistributionTransactionResourceRepository getDistributionTransactionResource() {
		if (Objects.isNull(distributionTransactionResourceRepository))
			synchronized (this) {
				distributionTransactionResourceRepository = meta
						.construct(DistributionTransactionResourceRepository.class);
			}
		return distributionTransactionResourceRepository;
	}

	public DistributionTransactionReleasesRepository getDistributionTransactionRelease() {
		if (Objects.isNull(distributionTransactionReleasesRepository))
			synchronized (this) {
				distributionTransactionReleasesRepository = meta
						.construct(DistributionTransactionReleasesRepository.class);
			}
		return distributionTransactionReleasesRepository;
	}

	public DistributionRepository getDistribution() {
		if (Objects.isNull(distributionRepository))
			synchronized (this) {
				distributionRepository = meta.construct(DistributionRepository.class);
			}
		return distributionRepository;
	}

	public Distribution1Repository getDistribution1() {
		if (Objects.isNull(distribution1Repository))
			synchronized (this) {
				distribution1Repository = meta.construct(Distribution1Repository.class);
			}
		return distribution1Repository;
	}

	public DistributionPagingRepository getDistributionPaging() {
		if (Objects.isNull(distributionPagingRepository))
			synchronized (this) {
				distributionPagingRepository = meta.construct(DistributionPagingRepository.class);
			}
		return distributionPagingRepository;
	}

	public ContentRefreshingTransactionsRepository getContentRefreshingTransactionRepository() {
		return transactionsRepository;
	}

	public ContentRefreshingTransactionDetailsRepository getContentRefreshingTransactionDetailsRepository() {
		return transactionDetailsRepository;
	}

	public ContentRefreshingTransactionStatusesRepository getContentRefreshingTransactionStatusesRepository() {
		return contentRefreshingTransactionStatusesRepository;
	}

	@Autowired
	public RepositoryFactory(ContentRefreshingTransactionsRepository transactionsRepository,
			ContentRefreshingTransactionDetailsRepository transactionDetailsRepository) {
		this.transactionsRepository = transactionsRepository;
		this.transactionDetailsRepository = transactionDetailsRepository;
	}

	public ReleaseResourceTagsRepo getReleaseResourceTags() {
		return releaseResourceTagsRepo;
	}

	public TechnicalSoundFileDetailRepository getTechnicalSoundFileRepository() {
		return technicalSoundFileDetailRepository;
	}

	public TechnicalImageFileDetailsRepository getTechnicalImageFileRepository() {
		return technicalImageFileDetailsRepository;
	}

	public TechnicalVideoFileDetailRepository getTechnicalVideoFileRepository() {
		return technicalVideoFileDetailRepository;
	}

	@Autowired
	private TechnicalVideoFileDetailRepository technicalVideoFileDetailRepository;

	@Autowired
	private TechnicalSoundFileDetailRepository technicalSoundFileDetailRepository;
	@Autowired
	private TechnicalImageFileDetailsRepository technicalImageFileDetailsRepository;
	@Autowired
	private ReleaseResourceTagsRepo releaseResourceTagsRepo;

	@Autowired
	private ContentRefreshingTransactionStatusesRepository contentRefreshingTransactionStatusesRepository;
	private ContentRefreshingTransactionDetailsRepository transactionDetailsRepository;
	private ContentRefreshingTransactionsRepository transactionsRepository;
	@Autowired
	private EntityManagerUtil meta;

	private DistributionActionsRepository distributionActionsRepository;

	private DistributionSourceRepository distributionSourceRepository;

	private DistributionTypeRepository distributionTypeRepository;

	private DistributionTransactionRepository distributionTransactionRepository;

	private DistributionTransactionResourceRepository distributionTransactionResourceRepository;

	private DistributionTransactionReleasesRepository distributionTransactionReleasesRepository;

	private DistributionRepository distributionRepository;

	private Distribution1Repository distribution1Repository;

	private DistributionPagingRepository distributionPagingRepository;
	@Autowired
	private DealRepository dealsRepository;
	@Autowired
	private DealTermsUsageUseTypesRepository dealTermsUsageUseTypesRepository;
	@Autowired
	private UseTypesRepository useTypesRepository;

}
