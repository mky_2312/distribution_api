/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.util;

/**
 * <p>The Interface Wait is used for making particular application,process,thread and etc to wait for a particular period of duration.<p>
 *
 * @author ankit
 */
public interface Wait {

	/** The one sec. */
	int ONE_SEC = 1000;

	/** The ten sec. */
	int TEN_SEC = 10 * ONE_SEC;

	/** The twenety sec. */
	int TWENETY_SEC = TEN_SEC + TEN_SEC;

	/** The one minute. */
	int ONE_MINUTE = TWENETY_SEC + TWENETY_SEC + TWENETY_SEC;

	/** The two minute. */
	int TWO_MINUTE = ONE_MINUTE + ONE_MINUTE;

}
