package com.spice.distribution.exceptions;

/**
 * @author ankit
 *
 */
public class DistributionException extends RuntimeException {

	/**
	 *  system generated serialVersionId.
	 */
	private static final long serialVersionUID = 213652124729930670L;

	public DistributionException(String message, Throwable exception) {
		super(message, exception);
	}

	public DistributionException(Throwable exception) {
		this(null, exception);
	}

	public DistributionException(String message) {
		this(message, null);
	}

}