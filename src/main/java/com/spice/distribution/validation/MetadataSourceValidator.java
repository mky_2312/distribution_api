package com.spice.distribution.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author ankit
 *
 */
public class MetadataSourceValidator implements ConstraintValidator<MetadataSource, String> {

	private volatile String cms;
	private volatile String ams;

	/* (non-Javadoc)
	 * @see javax.validation.ConstraintValidator#initialize(java.lang.annotation.Annotation)
	 */
	@Override
	public void initialize(MetadataSource constraintAnnotation) {
		cms = constraintAnnotation.CMS();
		ams = constraintAnnotation.AMS();
	}

	/* (non-Javadoc)
	 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object, javax.validation.ConstraintValidatorContext)
	 */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return cms.equalsIgnoreCase(value) || ams.equalsIgnoreCase(value);
	}

}
