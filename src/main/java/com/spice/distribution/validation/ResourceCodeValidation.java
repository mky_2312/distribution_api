package com.spice.distribution.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * @author ankit
 *
 */
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { ResourceCodeValidator.class })
public @interface ResourceCodeValidation {
	String message() default "Invalid Resource Code";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String CODE() default "";

}
