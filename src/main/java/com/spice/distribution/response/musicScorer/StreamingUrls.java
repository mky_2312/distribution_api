/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spice.distribution.response.musicScorer;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Class StreamingUrls representing StreamingUrls in Json.
 * 
 * @author ankit
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "FullHighDefinition", "HighDefinition","Standard", "Medium", "Low", "VeryLow","AdaptiveBitrate" })
public class StreamingUrls {

	/** The full high definition. */
	@JsonProperty("FullHighDefinition")
	private String fullHighDefinition;

	/** The high definition. */
	@JsonProperty("HighDefinition")
	private String highDefinition;

	@JsonProperty("Standard")
	private String standard;
	
	/** The medium. */
	@JsonProperty("Medium")
	private String medium;

	/** The low. */
	@JsonProperty("Low")
	private String low;
	
	@JsonProperty("VeryLow")
	private String veryLow;

	/** The adaptive bitrate. */
	@JsonProperty("AdaptiveBitrate")
	private String adaptiveBitrate;

	/** The additional properties. */
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Gets the full high definition.
	 *
	 * @return the full high definition
	 */
	@JsonProperty("FullHighDefinition")
	public String getFullHighDefinition() {
		return fullHighDefinition;
	}

	/**
	 * Sets the full high definition.
	 *
	 * @param fullHighDefinition the new full high definition
	 */
	@JsonProperty("FullHighDefinition")
	public void setFullHighDefinition(String fullHighDefinition) {
		this.fullHighDefinition = fullHighDefinition;
	}

	/**
	 * Gets the high definition.
	 *
	 * @return the high definition
	 */
	@JsonProperty("HighDefinition")
	public String getHighDefinition() {
		return highDefinition;
	}

	/**
	 * Sets the high definition.
	 *
	 * @param highDefinition the new high definition
	 */
	@JsonProperty("HighDefinition")
	public void setHighDefinition(String highDefinition) {
		this.highDefinition = highDefinition;
	}

	/**
	 * Gets the medium.
	 *
	 * @return the medium
	 */
	@JsonProperty("Medium")
	public String getMedium() {
		return medium;
	}

	/**
	 * Sets the medium.
	 *
	 * @param medium the new medium
	 */
	@JsonProperty("Medium")
	public void setMedium(String medium) {
		this.medium = medium;
	}

	/**
	 * Gets the low.
	 *
	 * @return the low
	 */
	@JsonProperty("Low")
	public String getLow() {
		return low;
	}

	/**
	 * Sets the low.
	 *
	 * @param low the new low
	 */
	@JsonProperty("Low")
	public void setLow(String low) {
		this.low = low;
	}

	/**
	 * Gets the adaptive bitrate.
	 *
	 * @return the adaptive bitrate
	 */
	@JsonProperty("AdaptiveBitrate")
	public String getAdaptiveBitrate() {
		return adaptiveBitrate;
	}

	/**
	 * Sets the adaptive bitrate.
	 *
	 * @param adaptiveBitrate the new adaptive bitrate
	 */
	@JsonProperty("AdaptiveBitrate")
	public void setAdaptiveBitrate(String adaptiveBitrate) {
		this.adaptiveBitrate = adaptiveBitrate;
	}

	/**
	 * Gets the additional properties.
	 *
	 * @return the additional properties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Sets the additional property.
	 *
	 * @param name the name
	 * @param value the value
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/**
	 * @return the standard
	 */
	@JsonProperty("Standard")
	public String getStandard() {
		return standard;
	}

	/**
	 * @param standard the standard to set
	 */
	@JsonProperty("Standard")
	public void setStandard(String standard) {
		this.standard = standard;
	}

	/**
	 * @return the veryLow
	 */
	@JsonProperty("VeryLow")
	public String getVeryLow() {
		return veryLow;
	}

	/**
	 * @param veryLow the veryLow to set
	 */
	@JsonProperty("VeryLow")
	public void setVeryLow(String veryLow) {
		this.veryLow = veryLow;
	}

	/**
	 * @param additionalProperties the additionalProperties to set
	 */
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

}
