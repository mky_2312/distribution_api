
package com.spice.distribution.response.metadata;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Codec", "Height", "Width", "FilePath", "FileName" })
public class TechnicalImageFileDetail {

	@JsonProperty("Codec")
	private String codec;
	@JsonProperty("Height")
	private Integer height;
	@JsonProperty("Width")
	private Integer width;
	@JsonProperty("FilePath")
	private String filePath;
	@JsonProperty("FileName")
	private String fileName;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("Codec")
	public String getCodec() {
		return codec;
	}

	@JsonProperty("Codec")
	public void setCodec(String codec) {
		this.codec = codec;
	}

	@JsonProperty("Height")
	public Integer getHeight() {
		return height;
	}

	@JsonProperty("Height")
	public void setHeight(Integer height) {
		this.height = height;
	}

	@JsonProperty("Width")
	public Integer getWidth() {
		return width;
	}

	@JsonProperty("Width")
	public void setWidth(Integer width) {
		this.width = width;
	}

	@JsonProperty("FilePath")
	public String getFilePath() {
		return filePath;
	}

	@JsonProperty("FilePath")
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@JsonProperty("FileName")
	public String getFileName() {
		return fileName;
	}

	@JsonProperty("FileName")
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TechnicalImageFileDetail [codec=" + codec + ", height=" + height + ", width=" + width + ", filePath="
				+ filePath + ", fileName=" + fileName + ", additionalProperties=" + additionalProperties + "]";
	}

}
