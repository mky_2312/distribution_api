
package com.spice.distribution.response.metadata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.spice.distribution.response.Response;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ResourceList" })
@XmlRootElement
public class MetadataResponse implements Response {

	@JsonProperty("ResourceList")
	private List<ResourceList> resourceList;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("ResourceList")
	public List<ResourceList> getResourceList() {
		return resourceList;
	}

	@JsonProperty("ResourceList")
	public void setResourceList(List<ResourceList> resourceList) {
		this.resourceList = resourceList;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MetadataResponse [resourceList=" + resourceList + ", additionalProperties=" + additionalProperties
				+ "]";
	}

}
