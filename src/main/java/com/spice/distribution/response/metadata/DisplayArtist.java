
package com.spice.distribution.response.metadata;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
// @JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Name", "Role", "DisplayArtistImageCode" })
public class DisplayArtist {

	@JsonProperty("Name")
	private String name;
	@JsonProperty("Role")
	private String role;
	@JsonProperty("DisplayArtistImageCode")
	private String displayArtistImageCode;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("Name")
	public String getName() {
		return name;
	}

	@JsonProperty("Name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("Role")
	public String getRole() {
		return role;
	}

	@JsonProperty("Role")
	public void setRole(String role) {
		this.role = role;
	}

	@JsonProperty("DisplayArtistImageCode")
	public String getDisplayArtistImageCode() {
		return displayArtistImageCode;
	}

	@JsonProperty("DisplayArtistImageCode")
	public void setDisplayArtistImageCode(String displayArtistImageCode) {
		this.displayArtistImageCode = displayArtistImageCode;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DisplayArtist [name=" + name + ", role=" + role + ", displayArtistImageCode=" + displayArtistImageCode
				+ ", additionalProperties=" + additionalProperties + "]";
	}

}
