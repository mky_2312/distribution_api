
package com.spice.distribution.response.metadata;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Title", "SubTitle", "UPC", "ReleaseType", "ReleaseImageCode" })
public class Release {

	@JsonProperty("Title")
	private String title;
	@JsonProperty("SubTitle")
	private String subTitle;
	@JsonProperty("UPC")
	private String uPC;
	@JsonProperty("ReleaseType")
	private String releaseType;
	@JsonProperty("ReleaseImageCode")
	private String releaseImageCode;

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("Title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("Title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("SubTitle")
	public String getSubTitle() {
		return subTitle;
	}

	@JsonProperty("SubTitle")
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	@JsonProperty("UPC")
	public String getUPC() {
		return uPC;
	}

	@JsonProperty("UPC")
	public void setUPC(String uPC) {
		this.uPC = uPC;
	}

	@JsonProperty("ReleaseType")
	public String getReleaseType() {
		return releaseType;
	}

	@JsonProperty("ReleaseType")
	public void setReleaseType(String releaseType) {
		this.releaseType = releaseType;
	}

	@JsonProperty("ReleaseImageCode")
	public String getReleaseImageCode() {
		return releaseImageCode;
	}

	@JsonProperty("ReleaseImageCode")
	public void setReleaseImageCode(String releaseImageCode) {
		this.releaseImageCode = releaseImageCode;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Release [title=" + title + ", subTitle=" + subTitle + ", uPC=" + uPC + ", releaseType=" + releaseType
				+ ", releaseImageCode=" + releaseImageCode + ", additionalProperties=" + additionalProperties + "]";
	}

}
