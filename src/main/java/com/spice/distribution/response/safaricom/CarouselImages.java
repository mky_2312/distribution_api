
package com.spice.distribution.response.safaricom;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "xs", "s", "m", "l" })

public class CarouselImages {

	@JsonProperty("xs")
	private String xs;
	@JsonProperty("s")
	private String s;
	@JsonProperty("m")
	private String m;
	@JsonProperty("l")
	private String l;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("xs")
	public String getXs() {
		return xs;
	}

	@JsonProperty("xs")
	public void setXs(String xs) {
		this.xs = xs;
	}

	@JsonProperty("s")
	public String getS() {
		return s;
	}

	@JsonProperty("s")
	public void setS(String s) {
		this.s = s;
	}

	@JsonProperty("m")
	public String getM() {
		return m;
	}

	@JsonProperty("m")
	public void setM(String m) {
		this.m = m;
	}

	@JsonProperty("l")
	public String getL() {
		return l;
	}

	@JsonProperty("l")
	public void setL(String l) {
		this.l = l;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

}
