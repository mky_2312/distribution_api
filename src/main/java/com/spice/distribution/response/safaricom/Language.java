
package com.spice.distribution.response.safaricom;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "safariId", "title", "code", "family" })
public class Language {

	@JsonProperty("safariId")
	private Integer safariId;
	@JsonProperty("title")
	private String title;
	@JsonProperty("code")
	private String code;
	@JsonProperty("family")
	private String family;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("safariId")
	public Integer getSafariId() {
		return safariId;
	}

	@JsonProperty("safariId")
	public void setSafariId(Integer safariId) {
		this.safariId = safariId;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	@JsonProperty("code")
	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("family")
	public String getFamily() {
		return family;
	}

	@JsonProperty("family")
	public void setFamily(String family) {
		this.family = family;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Language [safariId=" + safariId + ", title=" + title + ", code=" + code + ", family=" + family
				+ ", additionalProperties=" + additionalProperties + "]";
	}

}
