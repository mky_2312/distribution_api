package com.spice.distribution.response.safaricom;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "safariId", "resourceCode", "title","subTitle", "description", "skizaId", "languageId", "resourceTags","genreIds", "artistIds", "albumIds", "duration", "abrUrls", "hlsUrls", "mp3Urls", "rtspUrls", "carouselImages", "artworkImages", "isPublished" })
public class Audio {
	@JsonProperty("status")
	private boolean status;
	@JsonProperty("message")
	private String message;
	@JsonProperty("safariId")
	private Long safariId;
	@JsonProperty("resourceCode")
	private String resourceCode;
	@JsonProperty("title")
	private String title;
	@JsonProperty("subTitle")
	private String subTitle;
	@JsonProperty("description")
	private String description;
	@JsonProperty("skizaId")
	private Integer skizaId;
	@JsonProperty("languageId")
	private Integer languageId;
	@JsonProperty("resourceTags")
	private List<String> resourceTags;
	@JsonProperty("genreIds")
	private List<Integer> genreIds = null;
	@JsonProperty("artistIds")
	private List<Integer> artistIds = null;
	@JsonProperty("albumIds")
	private List<Integer> albumIds = null;
	@JsonProperty("duration")
	private Integer duration;
	@JsonProperty("abrUrls")
	private AbrUrls abrUrls;
	@JsonProperty("hlsUrls")
	private HlsUrls hlsUrls;
	@JsonProperty("mp3Urls")
	private Mp3Urls mp3Urls;
	@JsonProperty("rtspUrls")
	private RtspUrls rtspUrls;
	@JsonProperty("carouselImages")
	private CarouselImages carouselImages;
	@JsonProperty("artworkImages")
	private ArtworkImages artworkImages;
	@JsonProperty("isPublished")
	private Boolean isPublished;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("safariId")
	public Long getSafariId() {
		return safariId;
	}

	@JsonProperty("safariId")
	public void setSafariId(Long safariId) {
		this.safariId = safariId;
	}

	@JsonProperty("resourceCode")
	public String getResourceCode() {
		return resourceCode;
	}

	@JsonProperty("resourceCode")
	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("skizaId")
	public Integer getSkizaId() {
		return skizaId;
	}

	@JsonProperty("skizaId")
	public void setSkizaId(Integer skizaId) {
		this.skizaId = skizaId;
	}

	@JsonProperty("languageId")
	public Integer getLanguageId() {
		return languageId;
	}

	@JsonProperty("languageId")
	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	@JsonProperty("genreIds")
	public List<Integer> getGenreIds() {
		return genreIds;
	}

	@JsonProperty("genreIds")
	public void setGenreIds(List<Integer> genreIds) {
		this.genreIds = genreIds;
	}

	@JsonProperty("artistIds")
	public List<Integer> getArtistIds() {
		return artistIds;
	}

	@JsonProperty("artistIds")
	public void setArtistIds(List<Integer> artistIds) {
		this.artistIds = artistIds;
	}

	@JsonProperty("albumIds")
	public List<Integer> getAlbumIds() {
		return albumIds;
	}

	@JsonProperty("albumIds")
	public void setAlbumIds(List<Integer> albumIds) {
		this.albumIds = albumIds;
	}

	@JsonProperty("duration")
	public Integer getDuration() {
		return duration;
	}

	@JsonProperty("duration")
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@JsonProperty("abrUrls")
	public AbrUrls getAbrUrls() {
		return abrUrls;
	}

	@JsonProperty("abrUrls")
	public void setAbrUrls(AbrUrls abrUrls) {
		this.abrUrls = abrUrls;
	}

	@JsonProperty("hlsUrls")
	public HlsUrls getHlsUrls() {
		return hlsUrls;
	}

	@JsonProperty("hlsUrls")
	public void setHlsUrls(HlsUrls hlsUrls) {
		this.hlsUrls = hlsUrls;
	}

	@JsonProperty("mp3Urls")
	public Mp3Urls getMp3Urls() {
		return mp3Urls;
	}

	@JsonProperty("mp3Urls")
	public void setMp3Urls(Mp3Urls mp3Urls) {
		this.mp3Urls = mp3Urls;
	}

	@JsonProperty("rtspUrls")
	public RtspUrls getRtspUrls() {
		return rtspUrls;
	}

	@JsonProperty("rtspUrls")
	public void setRtspUrls(RtspUrls rtspUrls) {
		this.rtspUrls = rtspUrls;
	}

	@JsonProperty("carouselImages")
	public CarouselImages getCarouselImages() {
		return carouselImages;
	}

	@JsonProperty("carouselImages")
	public void setCarouselImages(CarouselImages carouselImages) {
		this.carouselImages = carouselImages;
	}

	@JsonProperty("artworkImages")
	public ArtworkImages getArtworkImages() {
		return artworkImages;
	}

	@JsonProperty("artworkImages")
	public void setArtworkImages(ArtworkImages artworkImages) {
		this.artworkImages = artworkImages;
	}

	@JsonProperty("isPublished")
	public Boolean getIsPublished() {
		return isPublished;
	}

	@JsonProperty("isPublished")
	public void setIsPublished(Boolean isPublished) {
		this.isPublished = isPublished;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getResourceTags() {
		return resourceTags;
	}

	public void setResourceTags(List<String> resourceTags) {
		this.resourceTags = resourceTags;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	/**
	 * @return the subTitle
	 */
	public String getSubTitle() {
		return subTitle;
	}

	/**
	 * @param subTitle the subTitle to set
	 */
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	/**
	 * @param status
	 * @param message
	 */
	public Audio(boolean status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	/**
	 * 
	 */
	public Audio() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Audio [status=" + status + ", message=" + message + ", safariId=" + safariId + ", resourceCode="
				+ resourceCode + ", title=" + title + ", subTitle=" + subTitle + ", description=" + description
				+ ", skizaId=" + skizaId + ", languageId=" + languageId + ", resourceTags=" + resourceTags
				+ ", genreIds=" + genreIds + ", artistIds=" + artistIds + ", albumIds=" + albumIds + ", duration="
				+ duration + ", abrUrls=" + abrUrls + ", hlsUrls=" + hlsUrls + ", mp3Urls=" + mp3Urls + ", rtspUrls="
				+ rtspUrls + ", carouselImages=" + carouselImages + ", artworkImages=" + artworkImages
				+ ", isPublished=" + isPublished + ", additionalProperties=" + additionalProperties + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	
	
	
	
}
