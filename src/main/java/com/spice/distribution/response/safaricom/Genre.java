
package com.spice.distribution.response.safaricom;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author ankit
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "safariId", "title", "carouselImages", "artworkImages" })

public class Genre {

	@JsonProperty("safariId")
	private Integer safariId;
	@JsonProperty("title")
	private String title;
	@JsonProperty("carouselImages")
	private CarouselImages carouselImages;
	@JsonProperty("artworkImages")
	private ArtworkImages artworkImages;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("safariId")
	public Integer getSafariId() {
		return safariId;
	}

	@JsonProperty("safariId")
	public void setSafariId(Integer safariId) {
		this.safariId = safariId;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("carouselImages")
	public CarouselImages getCarouselImages() {
		return carouselImages;
	}

	@JsonProperty("carouselImages")
	public void setCarouselImages(CarouselImages carouselImages) {
		this.carouselImages = carouselImages;
	}

	@JsonProperty("artworkImages")
	public ArtworkImages getArtworkImages() {
		return artworkImages;
	}

	@JsonProperty("artworkImages")
	public void setArtworkImages(ArtworkImages artworkImages) {
		this.artworkImages = artworkImages;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Genre [safariId=" + safariId + ", title=" + title + ", carouselImages=" + carouselImages
				+ ", artworkImages=" + artworkImages + ", additionalProperties=" + additionalProperties + "]";
	}

}
