/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spice.distribution.response.cnema;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Class Genre representing Genre in Json.
 * 
 * @author ankit
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Genre", "GenreImageUrls" })
public class Genre {

	/** The genre. */
	@JsonProperty("Genre")
	private String genre;

	/** The genre image urls. */
	@JsonProperty("GenreImageUrls")
	private GenreImageUrls genreImageUrls;

	/** The additional properties. */
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Gets the genre.
	 *
	 * @return the genre
	 */
	@JsonProperty("Genre")
	public String getGenre() {
		return genre;
	}

	/**
	 * Sets the genre.
	 *
	 * @param genre the new genre
	 */
	@JsonProperty("Genre")
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
	 * Gets the genre image urls.
	 *
	 * @return the genre image urls
	 */
	@JsonProperty("GenreImageUrls")
	public GenreImageUrls getGenreImageUrls() {
		return genreImageUrls;
	}

	/**
	 * Sets the genre image urls.
	 *
	 * @param genreImageUrls the new genre image urls
	 */
	@JsonProperty("GenreImageUrls")
	public void setGenreImageUrls(GenreImageUrls genreImageUrls) {
		this.genreImageUrls = genreImageUrls;
	}

	/**
	 * Gets the additional properties.
	 *
	 * @return the additional properties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Sets the additional property.
	 *
	 * @param name the name
	 * @param value the value
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Genre [genre=" + genre + ", genreImageUrls=" + genreImageUrls + ", additionalProperties="
				+ additionalProperties + "]";
	}

}
