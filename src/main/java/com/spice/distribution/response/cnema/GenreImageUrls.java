/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spice.distribution.response.cnema;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Class GenreImageUrls representing GenreImageUrls in Json.
 * 
 * @author ankit
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "xs", "s", "m", "l" })
public class GenreImageUrls {

	/** The xs. */
	@JsonProperty("xs")
	private String xs;

	/** The s. */
	@JsonProperty("s")
	private String s;

	/** The m. */
	@JsonProperty("m")
	private String m;

	/** The l. */
	@JsonProperty("l")
	private String l;

	/** The additional properties. */
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Gets the xs.
	 *
	 * @return the xs
	 */
	@JsonProperty("xs")
	public String getXs() {
		return xs;
	}

	/**
	 * Sets the xs.
	 *
	 * @param xs the new xs
	 */
	@JsonProperty("xs")
	public void setXs(String xs) {
		this.xs = xs;
	}

	/**
	 * Gets the s.
	 *
	 * @return the s
	 */
	@JsonProperty("s")
	public String getS() {
		return s;
	}

	/**
	 * Sets the s.
	 *
	 * @param s the new s
	 */
	@JsonProperty("s")
	public void setS(String s) {
		this.s = s;
	}

	/**
	 * Gets the m.
	 *
	 * @return the m
	 */
	@JsonProperty("m")
	public String getM() {
		return m;
	}

	/**
	 * Sets the m.
	 *
	 * @param m the new m
	 */
	@JsonProperty("m")
	public void setM(String m) {
		this.m = m;
	}

	/**
	 * Gets the l.
	 *
	 * @return the l
	 */
	@JsonProperty("l")
	public String getL() {
		return l;
	}

	/**
	 * Sets the l.
	 *
	 * @param l the new l
	 */
	@JsonProperty("l")
	public void setL(String l) {
		this.l = l;
	}

	/**
	 * Gets the additional properties.
	 *
	 * @return the additional properties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Sets the additional property.
	 *
	 * @param name the name
	 * @param value the value
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
