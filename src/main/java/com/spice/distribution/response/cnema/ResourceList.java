/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.spice.distribution.response.cnema;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Class ResourceList representing ResourceList In Json.
 * 
 * @author ankit
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ISRC", "ResourceCode", "ResourceType", "isPremium", "ResourceSubType", "PublicationDate", "Title", "SubTitle", "ContentPartner","UseTypes", "ResourceTags", "LanguageCodes", "CountryOfOrigin", "Duration", "TerritoryCodes", "Release", "DisplayArtists", "Genres", "StreamingUrls", "DownloadUrls" })
public class ResourceList {

	/** The status. */
	@JsonProperty("status")
	private boolean status;

	/** The message. */
	@JsonProperty("message")
	private String message;
	/** The i SRC. */
	@JsonProperty("ISRC")
	private String iSRC;
	@JsonProperty("PublicationDate")
	private String publicationDate;
	@JsonProperty("isPremium")
	private Byte isPremium;

	/** The resource code. */
	@JsonProperty("ResourceCode")
	private String resourceCode;

	/** The resource type. */
	@JsonProperty("ResourceType")
	private String resourceType;

	/** The resource sub type. */
	@JsonProperty("ResourceSubType")
	private String resourceSubType;

	/** The title. */
	@JsonProperty("Title")
	private String title;

	/** The sub title. */
	@JsonProperty("SubTitle")
	private Object subTitle;

	/** The content partner. */
	@JsonProperty("ContentPartner")
	private String contentPartner;
	@JsonProperty("UseTypes")
	private List<String> UseTypes;

	/** The resource tags. */
	@JsonProperty("ResourceTags")
	private List<String> resourceTags;

	/** The language codes. */
	@JsonProperty("LanguageCodes")
	private List<String> languageCodes = null;

	/** The country of origin. */
	@JsonProperty("CountryOfOrigin")
	private List<String> countryOfOrigin = null;

	/** The duration. */
	@JsonProperty("Duration")
	private Integer duration;

	/** The territory codes. */
	@JsonProperty("TerritoryCodes")
	private Set<String> territoryCodes = null;

	/** The release. */
	@JsonProperty("Release")
	private Release release;

	/** The display artists. */
	@JsonProperty("DisplayArtists")
	private List<DisplayArtist> displayArtists = null;

	/** The genres. */
	@JsonProperty("Genres")
	private List<Genre> genres = null;

	/** The streaming urls. */
	@JsonProperty("StreamingUrls")
	private StreamingUrls streamingUrls;

	/** The download urls. */
	@JsonProperty("DownloadUrls")
	private DownloadUrls downloadUrls;

	/** The additional properties. */
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Gets the isrc.
	 *
	 * @return the isrc
	 */
	@JsonProperty("ISRC")
	public String getISRC() {
		return iSRC;
	}

	/**
	 * Sets the isrc.
	 *
	 * @param iSRC
	 *            the new isrc
	 */
	@JsonProperty("ISRC")
	public void setISRC(String iSRC) {
		this.iSRC = iSRC;
	}

	/**
	 * Gets the resource code.
	 *
	 * @return the resource code
	 */
	@JsonProperty("ResourceCode")
	public String getResourceCode() {
		return resourceCode;
	}

	/**
	 * Sets the resource code.
	 *
	 * @param resourceCode
	 *            the new resource code
	 */
	@JsonProperty("ResourceCode")
	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	/**
	 * Gets the resource type.
	 *
	 * @return the resource type
	 */
	@JsonProperty("ResourceType")
	public String getResourceType() {
		return resourceType;
	}

	/**
	 * Sets the resource type.
	 *
	 * @param resourceType
	 *            the new resource type
	 */
	@JsonProperty("ResourceType")
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	@JsonProperty("Title")
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the new title
	 */
	@JsonProperty("Title")
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the sub title.
	 *
	 * @return the sub title
	 */
	@JsonProperty("SubTitle")
	public Object getSubTitle() {
		return subTitle;
	}

	/**
	 * Sets the sub title.
	 *
	 * @param subTitle
	 *            the new sub title
	 */
	@JsonProperty("SubTitle")
	public void setSubTitle(Object subTitle) {
		this.subTitle = subTitle;
	}

	/**
	 * Gets the content partner.
	 *
	 * @return the content partner
	 */
	@JsonProperty("ContentPartner")
	public String getContentPartner() {
		return contentPartner;
	}

	/**
	 * Sets the content partner.
	 *
	 * @param contentPartner
	 *            the new content partner
	 */
	@JsonProperty("ContentPartner")
	public void setContentPartner(String contentPartner) {
		this.contentPartner = contentPartner;
	}

	/**
	 * Gets the language codes.
	 *
	 * @return the language codes
	 */
	@JsonProperty("LanguageCodes")
	public List<String> getLanguageCodes() {
		return languageCodes;
	}

	/**
	 * Sets the language codes.
	 *
	 * @param languageCodes
	 *            the new language codes
	 */
	@JsonProperty("LanguageCodes")
	public void setLanguageCodes(List<String> languageCodes) {
		this.languageCodes = languageCodes;
	}

	/**
	 * Gets the country of origin.
	 *
	 * @return the country of origin
	 */
	@JsonProperty("CountryOfOrigin")
	public List<String> getCountryOfOrigin() {
		return countryOfOrigin;
	}

	/**
	 * Sets the country of origin.
	 *
	 * @param countryOfOrigin
	 *            the new country of origin
	 */
	@JsonProperty("CountryOfOrigin")
	public void setCountryOfOrigin(List<String> countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	/**
	 * Gets the duration.
	 *
	 * @return the duration
	 */
	@JsonProperty("Duration")
	public Integer getDuration() {
		return duration;
	}

	public String getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

	/**
	 * Sets the duration.
	 *
	 * @param duration
	 *            the new duration
	 */
	@JsonProperty("Duration")
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	/**
	 * Gets the territory codes.
	 *
	 * @return the territory codes
	 */
	@JsonProperty("TerritoryCodes")
	public Set<String> getTerritoryCodes() {
		return territoryCodes;
	}

	/**
	 * Sets the territory codes.
	 *
	 * @param territoryCodes
	 *            the new territory codes
	 */
	@JsonProperty("TerritoryCodes")
	public void setTerritoryCodes(Set<String> territoryCodes) {
		this.territoryCodes = territoryCodes;
	}

	/**
	 * Gets the release.
	 *
	 * @return the release
	 */
	@JsonProperty("Release")
	public Release getRelease() {
		return release;
	}

	/**
	 * Sets the release.
	 *
	 * @param release
	 *            the new release
	 */
	@JsonProperty("Release")
	public void setRelease(Release release) {
		this.release = release;
	}

	/**
	 * Gets the display artists.
	 *
	 * @return the display artists
	 */
	@JsonProperty("DisplayArtists")
	public List<DisplayArtist> getDisplayArtists() {
		return displayArtists;
	}

	/**
	 * Sets the display artists.
	 *
	 * @param displayArtists
	 *            the new display artists
	 */
	@JsonProperty("DisplayArtists")
	public void setDisplayArtists(List<DisplayArtist> displayArtists) {
		this.displayArtists = displayArtists;
	}

	/**
	 * Gets the genres.
	 *
	 * @return the genres
	 */
	@JsonProperty("Genres")
	public List<Genre> getGenres() {
		return genres;
	}

	/**
	 * Sets the genres.
	 *
	 * @param genres
	 *            the new genres
	 */
	@JsonProperty("Genres")
	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	/**
	 * Gets the streaming urls.
	 *
	 * @return the streaming urls
	 */
	@JsonProperty("StreamingUrls")
	public StreamingUrls getStreamingUrls() {
		return streamingUrls;
	}

	/**
	 * Sets the streaming urls.
	 *
	 * @param streamingUrls
	 *            the new streaming urls
	 */
	@JsonProperty("StreamingUrls")
	public void setStreamingUrls(StreamingUrls streamingUrls) {
		this.streamingUrls = streamingUrls;
	}

	/**
	 * Gets the download urls.
	 *
	 * @return the download urls
	 */
	@JsonProperty("DownloadUrls")
	public DownloadUrls getDownloadUrls() {
		return downloadUrls;
	}

	/**
	 * Sets the download urls.
	 *
	 * @param downloadUrls
	 *            the new download urls
	 */
	@JsonProperty("DownloadUrls")
	public void setDownloadUrls(DownloadUrls downloadUrls) {
		this.downloadUrls = downloadUrls;
	}

	/**
	 * Gets the additional properties.
	 *
	 * @return the additional properties
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Sets the additional property.
	 *
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/**
	 * Checks if is status.
	 *
	 * @return true, if is status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the i SRC.
	 *
	 * @return the i SRC
	 */
	public String getiSRC() {
		return iSRC;
	}

	/**
	 * Sets the i SRC.
	 *
	 * @param iSRC the new i SRC
	 */
	public void setiSRC(String iSRC) {
		this.iSRC = iSRC;
	}

	/**
	 * Gets the resource sub type.
	 *
	 * @return the resource sub type
	 */
	public String getResourceSubType() {
		return resourceSubType;
	}

	/**
	 * Sets the resource sub type.
	 *
	 * @param resourceSubType the new resource sub type
	 */
	public void setResourceSubType(String resourceSubType) {
		this.resourceSubType = resourceSubType;
	}

	public List<String> getResourceTags() {
		return resourceTags;
	}

	public void setResourceTags(List<String> resourceTags) {
		this.resourceTags = resourceTags;
	}

	/**
	 * Instantiates a new resource list.
	 *
	 * @param status the status
	 * @param message the message
	 */
	public ResourceList(boolean status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	/**
	 * Instantiates a new resource list.
	 */
	public ResourceList() {

	}

	public Byte getIsPremium() {
		return isPremium;
	}

	public void setIsPremium(Byte isPremium) {
		this.isPremium = isPremium;
	}

	
	

	/**
	 * @return the useTypes
	 */
	@JsonProperty("UseTypes")
	public List<String> getUseTypes() {
		return UseTypes;
	}

	/**
	 * @param useTypes the useTypes to set
	 */
	@JsonProperty("UseTypes")
	public void setUseTypes(List<String> useTypes) {
		UseTypes = useTypes;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResourceList [status=" + status + ", message=" + message + ", iSRC=" + iSRC + ", publicationDate="
				+ publicationDate + ", isPremium=" + isPremium + ", resourceCode=" + resourceCode + ", resourceType="
				+ resourceType + ", resourceSubType=" + resourceSubType + ", title=" + title + ", subTitle=" + subTitle
				+ ", contentPartner=" + contentPartner + ", UseTypes=" + UseTypes + ", resourceTags=" + resourceTags
				+ ", languageCodes=" + languageCodes + ", countryOfOrigin=" + countryOfOrigin + ", duration=" + duration
				+ ", territoryCodes=" + territoryCodes + ", release=" + release + ", displayArtists=" + displayArtists
				+ ", genres=" + genres + ", streamingUrls=" + streamingUrls + ", downloadUrls=" + downloadUrls
				+ "]";
	}

	
}
