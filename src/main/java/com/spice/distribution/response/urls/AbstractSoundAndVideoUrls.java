package com.spice.distribution.response.urls;

import java.util.List;
import java.util.Objects;

import com.spice.distribution.service.Url;
import com.spice.distribution.util.ObjectsUtil;
import com.spice.distribution.validation.EncryptResource;

/**
 * @author ankit
 *
 */
public final class AbstractSoundAndVideoUrls implements Url<UrlsGetter> {
	private final String MP4 = "mp4:";
	private final String SMIL = "smil:";
	private final String S3 = "amazons3";
	private String streamingBaseUrl = "http://54.76.12.125/vods3/_definst_/";
	private String downloadBaseUrl = "http://cdn.mziiki.com/";
	private String urlSeparator = "/";
	private final String filenameSeparator = "_";
	private String resourceCode;
	private List<String> resourceCodes;

	public AbstractSoundAndVideoUrls(String code) {
		this(null, null, code);
	}

	public AbstractSoundAndVideoUrls(String downloadBaseUrl, String streamingBaseUrl, String code) {
		if (Objects.nonNull(streamingBaseUrl))
			this.streamingBaseUrl = streamingBaseUrl;
		if (Objects.nonNull(downloadBaseUrl))
			this.downloadBaseUrl = downloadBaseUrl;
		this.resourceCode = code;
		this.resourceCodes = ObjectsUtil.breakResourceCode(code, "Invalid Resource Code : " + code);
	}

	@Override
	public UrlsGetter create(int serviceId) {
		UrlsGetter getter = new UrlsGetter();
		createStreamingUrls(getter);
		createDownloadUrls(getter);
		return getter;
	}

	private void createStreamingUrls(UrlsGetter getter) {
		getter.setFullHighDefinition(createStreamingUrls("197"));
		getter.setHighDefinition(createStreamingUrls("198"));
		getter.setStandards(createStreamingUrls("199"));
		getter.setMedium(createStreamingUrls("200"));
		getter.setLow(createStreamingUrls("201"));
		getter.setVeryLow(createStreamingUrls("202"));
		getter.setAdaptiveBitrate(createAdaptiveBitRate("Auto"));
	}

	private void createDownloadUrls(UrlsGetter getter) {
		getter.setStandard(createDownloadUrls("198"));
		getter.setDownloadMedium(createDownloadUrls("199"));
		getter.setDownloadLow(createDownloadUrls("201"));
	}

	private String createDownloadUrls(String quality) {
		StringBuilder builder = new StringBuilder(downloadBaseUrl);
		builder.append(resourceCodes.get(0));
		builder.append(urlSeparator);
		builder.append(resourceCodes.get(1));
		builder.append(urlSeparator);
		builder.append(resourceCodes.get(2));
		builder.append(urlSeparator);
		builder.append("03");
		builder.append(urlSeparator);
		builder.append(resourceCode);
		builder.append(filenameSeparator);
		builder.append("03");
		builder.append(filenameSeparator);
		builder.append(quality);
		builder.append(filenameSeparator);
		builder.append("videos.mp4");
		return builder.toString();
	}

	private String createStreamingUrls(String quality) {
		StringBuilder builder = new StringBuilder(streamingBaseUrl);
		builder.append(MP4);
		StringBuilder mediaWithoutEncrypt=new StringBuilder();
		mediaWithoutEncrypt.append(S3);
		mediaWithoutEncrypt.append(urlSeparator);
		mediaWithoutEncrypt.append(resourceCodes.get(0));
		mediaWithoutEncrypt.append(urlSeparator);
		mediaWithoutEncrypt.append(resourceCodes.get(1));
		mediaWithoutEncrypt.append(urlSeparator);
		mediaWithoutEncrypt.append(resourceCodes.get(2));
		mediaWithoutEncrypt.append(urlSeparator);
		mediaWithoutEncrypt.append("03");
		mediaWithoutEncrypt.append(urlSeparator);
		mediaWithoutEncrypt.append(resourceCode);
		mediaWithoutEncrypt.append(filenameSeparator);
		mediaWithoutEncrypt.append("03");
		mediaWithoutEncrypt.append(filenameSeparator);
		mediaWithoutEncrypt.append(quality);
		mediaWithoutEncrypt.append(filenameSeparator);
		mediaWithoutEncrypt.append("videos.mp4");
		builder.append(	EncryptResource.encrypt(mediaWithoutEncrypt.toString()));
		builder.append(urlSeparator);
		builder.append("playlist.m3u8");
		return builder.toString();
			}

	private String createAdaptiveBitRate(String quality) {
		StringBuilder builder = new StringBuilder(streamingBaseUrl);
		builder.append(SMIL);
		StringBuilder AdaptiveWithoutEncrypt=new StringBuilder();
		AdaptiveWithoutEncrypt.append(S3);
		AdaptiveWithoutEncrypt.append(urlSeparator);
		AdaptiveWithoutEncrypt.append(resourceCodes.get(0));
		AdaptiveWithoutEncrypt.append(urlSeparator);
		AdaptiveWithoutEncrypt.append(resourceCodes.get(1));
		AdaptiveWithoutEncrypt.append(urlSeparator);
		AdaptiveWithoutEncrypt.append(resourceCodes.get(2));
		AdaptiveWithoutEncrypt.append(urlSeparator);
		AdaptiveWithoutEncrypt.append("03");
		AdaptiveWithoutEncrypt.append(urlSeparator);
		AdaptiveWithoutEncrypt.append(resourceCode);
		AdaptiveWithoutEncrypt.append(filenameSeparator);
		AdaptiveWithoutEncrypt.append("Video");
		AdaptiveWithoutEncrypt.append(filenameSeparator);
		AdaptiveWithoutEncrypt.append(quality);
		AdaptiveWithoutEncrypt.append(".smil");
		builder.append(	EncryptResource.encrypt(AdaptiveWithoutEncrypt.toString()));
		builder.append(urlSeparator);
		builder.append("playlist.m3u8");
		return builder.toString();
	}

	public String createSoundRecodingUrl(String fileName,String filePath) {
		String finalpath=filePath.replaceFirst("Content/", "");
		StringBuilder builder = new StringBuilder(downloadBaseUrl);
		builder.append(finalpath);
		builder.append(fileName);
		return builder.toString();
	}
	
}
