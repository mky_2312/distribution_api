/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spice.distribution.response.urls;

/**
 *
 * @author jitender.kumar
 */
public class ArtworkImages {

	String xs;
	String s;
	String m;
	String l;

	public ArtworkImages(String imageCode) {
		try {
			if (imageCode.equalsIgnoreCase("7070872448")) {
				imageCode = "7070926185";
			}
			String imageUrl = "";

			imageUrl = "http://skcdn.scontentzone.com/" + imageCode.substring(0, 3) + "/" + imageCode.substring(3, 7) + "/" + imageCode.substring(7, 10) + "/08/";

			//imageUrl = imageUrl.substring(0, imageUrl.length() - 9);
			xs = imageUrl + imageCode + "08125_0.jpg";
			s = imageUrl + imageCode + "08126_0.jpg";
			m = imageUrl + imageCode + "08127_0.jpg";
			l = imageUrl + imageCode + "08128_0.jpg";
		} catch (Exception e) {

		}
	}

	public ArtworkImages(String xs, String s, String m, String l) {
		this.xs = xs;
		this.s = s;
		this.m = m;
		this.l = l;
	}

	public String getXs() {
		return xs;
	}

	public void setXs(String xs) {
		this.xs = xs;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public String getM() {
		return m;
	}

	public void setM(String m) {
		this.m = m;
	}

	public String getL() {
		return l;
	}

	public void setL(String l) {
		this.l = l;
	}

	//    public static void main (String args[]){
	//        ArtworkImages img=new ArtworkImages("7070872448");
	//         System.out.println(img.toJson(img));
	//    }
	//    public String toJson(Object object) {
	//        String result = null;
	//        ObjectMapper mapper = new ObjectMapper();
	//        try {
	//            result = mapper.writeValueAsString(object);
	//            return result;
	//        } catch (Exception e) {
	//            System.out.println("Exception in Mziiki MainServlet.toJson(Object object) - " + e.getMessage());
	//        }
	//        return null;
	//    }

}