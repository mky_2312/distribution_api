/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spice.distribution.response.urls;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author jitender.kumar
 */
public class AbrUrls {

	String auto;
	String high;
	String medium;
	String low;

	public AbrUrls() {
	}

	public void setAbrUrls(String resourceCode) {
		String path = "", streamUrl = "http://skabr.scontentzone.com:80/aod/_definst_/smil:";
		try {
			resourceCode = resourceCode.trim();

			path = resourceCode.substring(0, 3) + "/" + resourceCode.substring(3, 7) + "/" + resourceCode.substring(7, 10) + "/07/";

			byte[] message = ("amazons3/" + path + resourceCode + "_Auto.smil").getBytes("UTF-8");
			String encoded = DatatypeConverter.printBase64Binary(message);

			auto = streamUrl + encoded + "/playlist.m3u8";

			message = ("amazons3/" + path + resourceCode + "_High.smil").getBytes("UTF-8");
			encoded = DatatypeConverter.printBase64Binary(message);

			high = streamUrl + encoded + "/playlist.m3u8";

			message = ("amazons3/" + path + resourceCode + "_Medium.smil").getBytes("UTF-8");
			encoded = DatatypeConverter.printBase64Binary(message);

			medium = streamUrl + encoded + "/playlist.m3u8";

			message = ("amazons3/" + path + resourceCode + "_Low.smil").getBytes("UTF-8");
			encoded = DatatypeConverter.printBase64Binary(message);

			low = streamUrl + encoded + "/playlist.m3u8";

		} catch (Exception ex) {
			ex.printStackTrace();
			Logger.getLogger(AbrUrls.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public String getAuto() {
		return auto;
	}

	public void setAuto(String auto) {
		this.auto = auto;
	}

	public String getHigh() {
		return high;
	}

	public void setHigh(String high) {
		this.high = high;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getLow() {
		return low;
	}

	public void setLow(String low) {
		this.low = low;
	}

}
