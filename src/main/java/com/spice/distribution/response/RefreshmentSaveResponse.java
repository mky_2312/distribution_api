package com.spice.distribution.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author ankit
 *
 */
public class RefreshmentSaveResponse extends Respond {
	@JsonProperty
	private Integer transactionId;

	public RefreshmentSaveResponse(boolean status, String message, Integer transactionId) {
		super(status, message);
		this.transactionId = transactionId;
	}
}
