package com.spice.distribution.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RespondKaraoke implements Response {

	
	private boolean isKaraokeFileAvailable;
	private String resourceCode,message;
	private Integer status; 
	
	
	
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public RespondKaraoke(boolean isKaraokeFileAvailable) {
		this.isKaraokeFileAvailable = isKaraokeFileAvailable;
	}

	/**
	 * 
	 */
	public RespondKaraoke() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param status
	 * @param message
	 */
	public RespondKaraoke(Integer status, String message, boolean isKaraokeFileAvailable, String resourceCode) {
		super();
		this.status = status;
		this.message = message;
		this.isKaraokeFileAvailable = isKaraokeFileAvailable;
		this.resourceCode = resourceCode;
	}

	public boolean isKaraokeFileAvailable() {
		return isKaraokeFileAvailable;
	}

	public void setKaraokeFileAvailable(boolean isKaraokeFileAvailable) {
		this.isKaraokeFileAvailable = isKaraokeFileAvailable;
	}

	public String getResourceCode() {
		return resourceCode;
	}

	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}

	
}
