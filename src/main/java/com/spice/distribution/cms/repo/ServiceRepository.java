/*
* Copyright 2017 the original author or authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.spice.distribution.cms.entity.Services;

/**
 * The Interface for generic CRUD operations on a repository for a Service.
 *
 * @author ankit
 * @see com.spice.distribution.cms.entity.Services
 */
public interface ServiceRepository extends CrudRepository<Services, Integer> {
	
	/**
	 *  Retrieves a list of entity by its serviceTypeId and serviceCategoryId.
	 *
	 * @param typeId the type id
	 * @param categoryId the category id
	 * @return the list representing Services with the given param.
	 */
	List<Services> findByServiceTypeIdAndServiceCategoryId(Integer typeId, Integer categoryId);

}
