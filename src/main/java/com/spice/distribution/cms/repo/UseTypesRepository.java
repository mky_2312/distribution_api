package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.spice.distribution.cms.entity.UseTypes;

public interface UseTypesRepository  extends CrudRepository<UseTypes, Integer>{
UseTypes findUseTypeById(Integer id);
@Query(value="select useType.use_type from CMS.ReleaseResources as rr join CMS.Deals as deal on deal.release_id=rr.release_id join CMS.DealTermsUsageUseTypes as dtu on dtu.deal_id=deal.id join CMS.UseTypes as useType on useType.id=dtu.use_type_id\n" + 
		"where rr.id=:releaseResourceId",nativeQuery=true)
List<String> useType(@Param("releaseResourceId")Integer releaseResourceId);
}
