/*
* Copyright 2017 the original author or authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.spice.distribution.cms.entity.DisplayArtists;

/**
 * The Interface for generic CRUD operations on a repository for a DisplayArtist.
 *
 * @author ankit
 * @see com.spice.distribution.cms.entity.DisplayArtists
 */
public interface DisplayArtistRepository extends CrudRepository<DisplayArtists, Integer> {
	
	/**
	 * Retrieves a list of entity by its release resource id.
	 *
	 * @param releaseResourceId the release resource id
	 * @return the list  containing array of Objects from DisplayArtists.
	 */
	@Query(value = "SELECT t1.full_name,t1.artist_image_resource_code FROM DisplayArtists t1  join  ReleaseResourceDisplayArtists t2 on t2.display_artist_id=t1.id  where t2.release_resource_id =:id", nativeQuery = true)
	List<Object[]> findByReleaseResourceId(@Param("id") int releaseResourceId);

	/**
	 * Retrieves a list of entity by its distribution Id.
	 *
	 * @param distributionId the distribution id
	 * @return the list  containing array of Objects from DisplayArtists.
	 */
	@Query(value = "SELECT t4.id,t4.full_name,t5.artist_role,t4.artist_image_resource_code FROM DISTRIBUTION.DistributionTransactionResources t1 JOIN CMS.ReleaseResources t2 ON t1.resource_code=t2.resource_code JOIN CMS.ReleaseResourceDisplayArtists t3 ON t2.id=t3.release_resource_id JOIN CMS.DisplayArtists t4 ON t3.display_artist_id=t4.id JOIN CMS.ArtistRoles t5 ON t5.id=t3.artist_role_id WHERE t1.distribution_transaction_id=:id", nativeQuery = true)
	List<Object[]> findByDistributionId(@Param("id") int distributionId);
}
