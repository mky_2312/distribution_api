/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spice.distribution.cms.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.spice.distribution.cms.entity.ContentRefreshingTransactions;

/**
 *  Interface for generic CRUD operations on a repository for a ContentRefreshingTransactions.
 *
 * @author ankit
 * @see com.spice.distribution.cms.entity.ContentRefreshingTransactions
 */
@Transactional
public interface ContentRefreshingTransactionsRepository extends PagingAndSortingRepository<ContentRefreshingTransactions, Integer>, CrudRepository<ContentRefreshingTransactions, Integer> {

	/**
	 * Sets the status.
	 *
	 * @param transactionId the transaction id
	 * @param oldStatusId the old status id
	 * @param newStausId the new staus id
	 */
	@Modifying
	@Query("update ContentRefreshingTransactions t set t.contentRefreshingTransactionStatusId =:newStatusId where t.contentRefreshingTransactionStatusId =:oldStatusId AND t.id =:id")
	void setStatus(@Param("id") int transactionId, @Param("oldStatusId") int oldStatusId, @Param("newStatusId") int newStausId);

	/**
	 * Checks if is transaction active.
	 *
	 * @param userId the user id
	 * @param countryId the country id
	 * @param countryName the country name
	 * @param serviceId the service id
	 * @param serviceName the service name
	 * @param containerId the container id
	 * @param containerName the container name
	 * @param id the id
	 * @return the list containing id if it is active.
	 */
	//	@Query(value = "select id from ContentRefreshingTransactions  where/ user_id =:userId AND country_id =:countryId AND country_name =:countryName AND service_id  =:serviceId  AND  service_name =:serviceName AND container_id =:containerId  AND container_name =:containerName AND content_refreshing_transaction_status_id in (:ids)", nativeQuery = true)
	@Query(value = "select id from ContentRefreshingTransactions where  country_id =:countryId AND country_name =:countryName AND service_id  =:serviceId  AND  service_name =:serviceName AND container_id =:containerId  AND container_name =:containerName AND content_refreshing_transaction_status_id in (:ids)", nativeQuery = true)
	List<Object[]> isTransactionValid(@Param("countryId") Integer countryId, @Param("countryName") String countryName, @Param("serviceId") Integer serviceId, @Param("serviceName") String serviceName, @Param("containerId") Integer containerId, @Param("containerName") String containerName, @Param("ids") List<Integer> id);

	/**
	 * Find by content refreshing transaction statuses id.
	 *
	 * @param id the id the given id.
	 * @return the list representing ContentRefreshingTransactions with the given param.
	 */
	List<ContentRefreshingTransactions> findByContentRefreshingTransactionStatusId(int id);
	
	@Query(value = "SELECT * from ContentRefreshingTransactions WHERE user_id=:userId ORDER BY id LIMIT :size OFFSET :offset", nativeQuery = true)
	List<ContentRefreshingTransactions> getRefreshingTransactionsByUserId(@Param("userId") int userId, @Param("offset") int offset, @Param("size") int size);

	@Modifying
	@Query(value = "update ContentRefreshingTransactions set content_refreshing_transaction_status_id =:statusId where id =:id", nativeQuery = true)
	void updateStatus(@Param("statusId") int statusId, @Param("id") int id);
	
	@Query(value = "SELECT * from ContentRefreshingTransactions WHERE service_name=:serviceName ORDER BY id LIMIT :size OFFSET :offset", nativeQuery = true)
	List<ContentRefreshingTransactions> getRefreshingTransactionsByServiceName(@Param("serviceName") String serviceName, @Param("offset") int offset, @Param("size") int size);
}
