package com.spice.distribution.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "ReleaseTypes")
public class ReleaseTypes {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "release_type", columnDefinition = "VARCHAR")
	private String release_type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRelease_type() {
		return release_type;
	}

	public void setRelease_type(String release_type) {
		this.release_type = release_type;
	}

	@Override
	public String toString() {
		return "ReleaseTypes [id=" + id + ", release_type=" + release_type + "]";
	}

}
