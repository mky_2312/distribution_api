package com.spice.distribution.cms.entity;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;



//@JsonInclude(JsonInclude.Include.NON_NULL)
/**
 * @author rahul 
 * @email  <rahul.gupta@approutes.com>
 * @date   10-Oct-2017
 */
@JsonPropertyOrder({ "result", "message" })
public class Response {

	@JsonProperty("result")
	private Boolean result;
	@JsonProperty("message")
	private String message;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("result")
	public Boolean getResult() {
		return result;
	}

	@JsonProperty("result")
	public void setResult(Boolean result) {
		this.result = result;
	}

	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	@JsonProperty("message")
	public void setMessage(String message) {
		this.message = message;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "Response [result=" + result + ", message=" + message + ", additionalProperties=" + additionalProperties + "]";
	}

}