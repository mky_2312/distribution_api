/*
* Copyright 2017 the original author or authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.spice.distribution.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



/**
 * The Class SoundRecordingTypes.
 * @author ankit
 * 
 */
@Entity
public class SoundRecordingTypes {
	
	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	/** The sound recording type. */
	@Column
	private String sound_recording_type;
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Gets the sound recording type.
	 *
	 * @return the sound recording type
	 */
	public String getSound_recording_type() {
		return sound_recording_type;
	}
	
	/**
	 * Sets the sound recording type.
	 *
	 * @param sound_recording_type the new sound recording type
	 */
	public void setSound_recording_type(String sound_recording_type) {
		this.sound_recording_type = sound_recording_type;
	}

	
}
