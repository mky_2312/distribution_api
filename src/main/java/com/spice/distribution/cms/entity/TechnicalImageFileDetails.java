package com.spice.distribution.cms.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "TechnicalImageFileDetails")
public class TechnicalImageFileDetails implements Cloneable {
	@Id
	private int id;
	@Column
	private Integer image_id;
	@Column
	private Integer image_details_by_territory_id;
	@Column
	private Integer technical_image_details_id;
	@Column
	private String technical_resource_details_reference;
	@Column
	private String file_name;
	@Column
	private String file_path;
	@Column
	private Integer technical_file_type_id;
	@Column
	private Integer old_technical_image_details_id;
	@Column
	private Long file_size;
	@Column
	private Long duration;
	@Column
	private Timestamp updated_at;
	@Column
	private Byte is_file_updated;
	@ManyToOne
	@JoinColumn(name = "technical_image_details_id", insertable = false, updatable = false)
	private TechnicalImageDetails technicalImageDetails;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getImage_id() {
		return image_id;
	}

	public void setImage_id(Integer image_id) {
		this.image_id = image_id;
	}

	public Integer getImage_details_by_territory_id() {
		return image_details_by_territory_id;
	}

	public void setImage_details_by_territory_id(Integer image_details_by_territory_id) {
		this.image_details_by_territory_id = image_details_by_territory_id;
	}

	public Integer getTechnical_image_details_id() {
		return technical_image_details_id;
	}

	public void setTechnical_image_details_id(Integer technical_image_details_id) {
		this.technical_image_details_id = technical_image_details_id;
	}

	public String getTechnical_resource_details_reference() {
		return technical_resource_details_reference;
	}

	public void setTechnical_resource_details_reference(String technical_resource_details_reference) {
		this.technical_resource_details_reference = technical_resource_details_reference;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public Integer getTechnical_file_type_id() {
		return technical_file_type_id;
	}

	public void setTechnical_file_type_id(Integer technical_file_type_id) {
		this.technical_file_type_id = technical_file_type_id;
	}

	public Integer getOld_technical_image_details_id() {
		return old_technical_image_details_id;
	}

	public void setOld_technical_image_details_id(Integer old_technical_image_details_id) {
		this.old_technical_image_details_id = old_technical_image_details_id;
	}

	public Long getFile_size() {
		return file_size;
	}

	public void setFile_size(Long file_size) {
		this.file_size = file_size;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Byte getIs_file_updated() {
		return is_file_updated;
	}

	public void setIs_file_updated(Byte is_file_updated) {
		this.is_file_updated = is_file_updated;
	}

	@Override
	public TechnicalImageFileDetails clone() throws CloneNotSupportedException {
		return (TechnicalImageFileDetails) super.clone();
	}

	public TechnicalImageDetails getTechnicalImageDetails() {
		return technicalImageDetails;
	}

	public void setTechnicalImageDetails(TechnicalImageDetails technicalImageDetails) {
		this.technicalImageDetails = technicalImageDetails;
	}

}
