package com.spice.distribution.cms.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * @author ankit
 *
 */
@Entity
public class ContentPartners {
	@Id
	private int id;
	private Integer content_partner_type_id;
	private String content_partner;
	private String name_on_invoice;
	private String tin;
	private Integer country_id;
	@OneToOne
	@JoinColumn(name = "country_id", insertable = false, updatable = false)
	private Countries countries;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getContent_partner_type_id() {
		return content_partner_type_id;
	}

	public void setContent_partner_type_id(Integer content_partner_type_id) {
		this.content_partner_type_id = content_partner_type_id;
	}

	public String getContent_partner() {
		return content_partner;
	}

	public void setContent_partner(String content_partner) {
		this.content_partner = content_partner;
	}

	public String getName_on_invoice() {
		return name_on_invoice;
	}

	public void setName_on_invoice(String name_on_invoice) {
		this.name_on_invoice = name_on_invoice;
	}

	public String getTin() {
		return tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public Integer getCountry_id() {
		return country_id;
	}

	public void setCountry_id(Integer country_id) {
		this.country_id = country_id;
	}

	public Countries getCountries() {
		return countries;
	}

	public void setCountries(Countries countries) {
		this.countries = countries;
	}

	@Override
	public String toString() {
		return "ContentPartners [id=" + id + ", content_partner_type_id=" + content_partner_type_id + ", content_partner=" + content_partner + ", name_on_invoice=" + name_on_invoice + ", tin=" + tin + "]";
	}
}