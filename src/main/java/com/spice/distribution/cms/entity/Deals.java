package com.spice.distribution.cms.entity;

import java.math.BigInteger;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "Deals")
public class Deals implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "release_id")
	private Integer releaseId;
	@Type(type = "numeric_boolean")
	@Column
	private Boolean is_pre_order_deal;
	@Type(type = "numeric_boolean")
	@Column
	private Boolean all_deals_cancelled;
	@Type(type = "numeric_boolean")
	@Column
	private Boolean is_promotional;
	@Column
	private String promotional_code;
	@Column
	private String consumer_rental_period;
	@Type(type = "numeric_boolean")
	@Column
	private Boolean consumer_rental_period_is_extensible;
	@Column
	private String pre_order_release_date;
	@Column
	private String release_display_start_date;
	@Column
	private String track_listing_preview_start_date;
	@Column
	private String cover_art_preview_start_date;
	@Column
	private String clip_preview_start_date;
	@Column
	private String release_display_start_date_time;
	@Column
	private String track_listing_preview_start_date_time;
	@Column
	private String cover_art_preview_start_date_time;
	@Column
	private String clip_preview_start_date_time;
	@Column
	private String pre_order_preview_date;
	@Column
	private String pre_order_preview_date_time;
	@Column
	private String pre_order_incentive_period_start_date;
	@Column
	private String pre_order_incentive_period_end_date;
	@Column
	private Date pre_order_incentive_period_start_date_time;
	@Column
	private Date pre_order_incentive_period_end_date_time;
	@Column
	private String instant_gratification_period_start_date;
	@Column
	private String instant_gratification_period_end_date;
	@Column
	private Date instant_gratification_period_start_date_time;
	@Column
	private Date instant_gratification_period_end_date_time;
	@Type(type = "numeric_boolean")
	@Column
	private Boolean is_exclusive;
	@Type(type = "numeric_boolean")
	@Column
	private Boolean physical_returns_allowed;
	@Column
	private String latest_date_for_physical_returns;
	@Column
	private BigInteger number_of_products_per_carton;
	@Column
	private String deal_terms_language_and_script_code;
	@Column
	private String language_and_script_code;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "deal_id")
	private Set<DealTermsTerritoryCodes> dealTermsTerritoryCodes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getIs_pre_order_deal() {
		return is_pre_order_deal;
	}

	public void setIs_pre_order_deal(Boolean is_pre_order_deal) {
		this.is_pre_order_deal = is_pre_order_deal;
	}

	public Boolean getAll_deals_cancelled() {
		return all_deals_cancelled;
	}

	public void setAll_deals_cancelled(Boolean all_deals_cancelled) {
		this.all_deals_cancelled = all_deals_cancelled;
	}

	public Boolean getIs_promotional() {
		return is_promotional;
	}

	public void setIs_promotional(Boolean is_promotional) {
		this.is_promotional = is_promotional;
	}

	public String getPromotional_code() {
		return promotional_code;
	}

	public void setPromotional_code(String promotional_code) {
		this.promotional_code = promotional_code;
	}

	public String getConsumer_rental_period() {
		return consumer_rental_period;
	}

	public void setConsumer_rental_period(String consumer_rental_period) {
		this.consumer_rental_period = consumer_rental_period;
	}

	public Boolean getConsumer_rental_period_is_extensible() {
		return consumer_rental_period_is_extensible;
	}

	public void setConsumer_rental_period_is_extensible(Boolean consumer_rental_period_is_extensible) {
		this.consumer_rental_period_is_extensible = consumer_rental_period_is_extensible;
	}

	public String getPre_order_release_date() {
		return pre_order_release_date;
	}

	public void setPre_order_release_date(String pre_order_release_date) {
		this.pre_order_release_date = pre_order_release_date;
	}

	public String getRelease_display_start_date() {
		return release_display_start_date;
	}

	public void setRelease_display_start_date(String release_display_start_date) {
		this.release_display_start_date = release_display_start_date;
	}

	public String getTrack_listing_preview_start_date() {
		return track_listing_preview_start_date;
	}

	public void setTrack_listing_preview_start_date(String track_listing_preview_start_date) {
		this.track_listing_preview_start_date = track_listing_preview_start_date;
	}

	public String getCover_art_preview_start_date() {
		return cover_art_preview_start_date;
	}

	public void setCover_art_preview_start_date(String cover_art_preview_start_date) {
		this.cover_art_preview_start_date = cover_art_preview_start_date;
	}

	public String getClip_preview_start_date() {
		return clip_preview_start_date;
	}

	public void setClip_preview_start_date(String clip_preview_start_date) {
		this.clip_preview_start_date = clip_preview_start_date;
	}

	public String getRelease_display_start_date_time() {
		return release_display_start_date_time;
	}

	public void setRelease_display_start_date_time(String release_display_start_date_time) {
		this.release_display_start_date_time = release_display_start_date_time;
	}

	public String getTrack_listing_preview_start_date_time() {
		return track_listing_preview_start_date_time;
	}

	public void setTrack_listing_preview_start_date_time(String track_listing_preview_start_date_time) {
		this.track_listing_preview_start_date_time = track_listing_preview_start_date_time;
	}

	public String getCover_art_preview_start_date_time() {
		return cover_art_preview_start_date_time;
	}

	public void setCover_art_preview_start_date_time(String cover_art_preview_start_date_time) {
		this.cover_art_preview_start_date_time = cover_art_preview_start_date_time;
	}

	public String getClip_preview_start_date_time() {
		return clip_preview_start_date_time;
	}

	public void setClip_preview_start_date_time(String clip_preview_start_date_time) {
		this.clip_preview_start_date_time = clip_preview_start_date_time;
	}

	public String getPre_order_preview_date() {
		return pre_order_preview_date;
	}

	public void setPre_order_preview_date(String pre_order_preview_date) {
		this.pre_order_preview_date = pre_order_preview_date;
	}

	public String getPre_order_preview_date_time() {
		return pre_order_preview_date_time;
	}

	public void setPre_order_preview_date_time(String pre_order_preview_date_time) {
		this.pre_order_preview_date_time = pre_order_preview_date_time;
	}

	public String getPre_order_incentive_period_start_date() {
		return pre_order_incentive_period_start_date;
	}

	public void setPre_order_incentive_period_start_date(String pre_order_incentive_period_start_date) {
		this.pre_order_incentive_period_start_date = pre_order_incentive_period_start_date;
	}

	public String getPre_order_incentive_period_end_date() {
		return pre_order_incentive_period_end_date;
	}

	public void setPre_order_incentive_period_end_date(String pre_order_incentive_period_end_date) {
		this.pre_order_incentive_period_end_date = pre_order_incentive_period_end_date;
	}

	public Date getPre_order_incentive_period_start_date_time() {
		return pre_order_incentive_period_start_date_time;
	}

	public void setPre_order_incentive_period_start_date_time(Date pre_order_incentive_period_start_date_time) {
		this.pre_order_incentive_period_start_date_time = pre_order_incentive_period_start_date_time;
	}

	public Date getPre_order_incentive_period_end_date_time() {
		return pre_order_incentive_period_end_date_time;
	}

	public void setPre_order_incentive_period_end_date_time(Date pre_order_incentive_period_end_date_time) {
		this.pre_order_incentive_period_end_date_time = pre_order_incentive_period_end_date_time;
	}

	public String getInstant_gratification_period_start_date() {
		return instant_gratification_period_start_date;
	}

	public void setInstant_gratification_period_start_date(String instant_gratification_period_start_date) {
		this.instant_gratification_period_start_date = instant_gratification_period_start_date;
	}

	public String getInstant_gratification_period_end_date() {
		return instant_gratification_period_end_date;
	}

	public void setInstant_gratification_period_end_date(String instant_gratification_period_end_date) {
		this.instant_gratification_period_end_date = instant_gratification_period_end_date;
	}

	public Date getInstant_gratification_period_start_date_time() {
		return instant_gratification_period_start_date_time;
	}

	public void setInstant_gratification_period_start_date_time(Date instant_gratification_period_start_date_time) {
		this.instant_gratification_period_start_date_time = instant_gratification_period_start_date_time;
	}

	public Date getInstant_gratification_period_end_date_time() {
		return instant_gratification_period_end_date_time;
	}

	public void setInstant_gratification_period_end_date_time(Date instant_gratification_period_end_date_time) {
		this.instant_gratification_period_end_date_time = instant_gratification_period_end_date_time;
	}

	public Boolean getIs_exclusive() {
		return is_exclusive;
	}

	public void setIs_exclusive(Boolean is_exclusive) {
		this.is_exclusive = is_exclusive;
	}

	public Boolean getPhysical_returns_allowed() {
		return physical_returns_allowed;
	}

	public void setPhysical_returns_allowed(Boolean physical_returns_allowed) {
		this.physical_returns_allowed = physical_returns_allowed;
	}

	public String getLatest_date_for_physical_returns() {
		return latest_date_for_physical_returns;
	}

	public void setLatest_date_for_physical_returns(String latest_date_for_physical_returns) {
		this.latest_date_for_physical_returns = latest_date_for_physical_returns;
	}

	public BigInteger getNumber_of_products_per_carton() {
		return number_of_products_per_carton;
	}

	public void setNumber_of_products_per_carton(BigInteger number_of_products_per_carton) {
		this.number_of_products_per_carton = number_of_products_per_carton;
	}

	public String getDeal_terms_language_and_script_code() {
		return deal_terms_language_and_script_code;
	}

	public void setDeal_terms_language_and_script_code(String deal_terms_language_and_script_code) {
		this.deal_terms_language_and_script_code = deal_terms_language_and_script_code;
	}

	public String getLanguage_and_script_code() {
		return language_and_script_code;
	}

	public void setLanguage_and_script_code(String language_and_script_code) {
		this.language_and_script_code = language_and_script_code;
	}

	public Integer getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(Integer releaseId) {
		this.releaseId = releaseId;
	}

	public Set<DealTermsTerritoryCodes> getDealTermsTerritoryCodes() {
		return dealTermsTerritoryCodes;
	}

	public void setDealTermsTerritoryCodes(Set<DealTermsTerritoryCodes> dealTermsTerritoryCodes) {
		this.dealTermsTerritoryCodes = dealTermsTerritoryCodes;
	}

	@Override
	public Deals clone() throws CloneNotSupportedException {
		return (Deals) super.clone();
	}

	@Override
	public String toString() {
		return "Deals [id=" + id + ", releaseId=" + releaseId + ", is_pre_order_deal=" + is_pre_order_deal + ", all_deals_cancelled=" + all_deals_cancelled + ", is_promotional=" + is_promotional + ", promotional_code=" + promotional_code + ", consumer_rental_period=" + consumer_rental_period + ", consumer_rental_period_is_extensible=" + consumer_rental_period_is_extensible + ", pre_order_release_date=" + pre_order_release_date + ", release_display_start_date=" + release_display_start_date
				+ ", track_listing_preview_start_date=" + track_listing_preview_start_date + ", cover_art_preview_start_date=" + cover_art_preview_start_date + ", clip_preview_start_date=" + clip_preview_start_date + ", release_display_start_date_time=" + release_display_start_date_time + ", track_listing_preview_start_date_time=" + track_listing_preview_start_date_time + ", cover_art_preview_start_date_time=" + cover_art_preview_start_date_time + ", clip_preview_start_date_time="
				+ clip_preview_start_date_time + ", pre_order_preview_date=" + pre_order_preview_date + ", pre_order_preview_date_time=" + pre_order_preview_date_time + ", pre_order_incentive_period_start_date=" + pre_order_incentive_period_start_date + ", pre_order_incentive_period_end_date=" + pre_order_incentive_period_end_date + ", pre_order_incentive_period_start_date_time=" + pre_order_incentive_period_start_date_time + ", pre_order_incentive_period_end_date_time="
				+ pre_order_incentive_period_end_date_time + ", instant_gratification_period_start_date=" + instant_gratification_period_start_date + ", instant_gratification_period_end_date=" + instant_gratification_period_end_date + ", instant_gratification_period_start_date_time=" + instant_gratification_period_start_date_time + ", instant_gratification_period_end_date_time=" + instant_gratification_period_end_date_time + ", is_exclusive=" + is_exclusive + ", physical_returns_allowed="
				+ physical_returns_allowed + ", latest_date_for_physical_returns=" + latest_date_for_physical_returns + ", number_of_products_per_carton=" + number_of_products_per_carton + ", deal_terms_language_and_script_code=" + deal_terms_language_and_script_code + ", language_and_script_code=" + language_and_script_code + ", dealTermsTerritoryCodes=" + dealTermsTerritoryCodes + "]";
	}

}
