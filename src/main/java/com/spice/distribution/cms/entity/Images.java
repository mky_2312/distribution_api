package com.spice.distribution.cms.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "Images")
public class Images implements Cloneable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private Integer content_partner_id;
	@Column
	private Integer release_resource_id;
	@Column
	private String proprietary_id;
	@Column
	private int image_type_id;
	@Column
	private Character is_artist_related;
	@Column
	private String resource_reference;
	@Column
	private String creation_date;
	@Column
	private Character creation_date_is_approximate;
	@Column
	private Character creation_date_is_before;
	@Column
	private Character creation_date_is_after;
	@Column
	private String creation_date_territory_code;
	@Column
	private String creation_date_location_description;
	@Column
	private String creation_date_language_and_script_code;
	@Column
	private Character is_updated;
	@Column
	private String language_and_script_code;
	@Column
	private String title_text;
	@Column
	private String sub_title;
	@Transient
	private String resource_code;
	@ManyToOne
	@JoinColumn(name = "id", insertable = false, updatable = false)
	private ReleaseResources releaseResources;
	@OneToMany
	@JoinColumn(name = "image_id", insertable = false, updatable = false)
	private List<TechnicalImageFileDetails> technicalImageFileDetails;
	@OneToOne
	@JoinColumn(name = "image_type_id", insertable = false, updatable = false)
	private ImageTypes imageTypes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getContent_partner_id() {
		return content_partner_id;
	}

	public void setContent_partner_id(Integer content_partner_id) {
		this.content_partner_id = content_partner_id;
	}

	public Integer getRelease_resource_id() {
		return release_resource_id;
	}

	public void setRelease_resource_id(Integer release_resource_id) {
		this.release_resource_id = release_resource_id;
	}

	public String getProprietary_id() {
		return proprietary_id;
	}

	public void setProprietary_id(String proprietary_id) {
		this.proprietary_id = proprietary_id;
	}

	public int getImage_type_id() {
		return image_type_id;
	}

	public void setImage_type_id(int image_type_id) {
		this.image_type_id = image_type_id;
	}

	public Character getIs_artist_related() {
		return is_artist_related;
	}

	public void setIs_artist_related(Character is_artist_related) {
		this.is_artist_related = is_artist_related;
	}

	public String getResource_reference() {
		return resource_reference;
	}

	public List<TechnicalImageFileDetails> getTechnicalImageFileDetails() {
		return technicalImageFileDetails;
	}

	public void setTechnicalImageFileDetails(List<TechnicalImageFileDetails> technicalImageFileDetails) {
		this.technicalImageFileDetails = technicalImageFileDetails;
	}

	public void setResource_reference(String resource_reference) {
		this.resource_reference = resource_reference;
	}

	public String getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(String creation_date) {
		this.creation_date = creation_date;
	}

	public Character getCreation_date_is_approximate() {
		return creation_date_is_approximate;
	}

	public void setCreation_date_is_approximate(Character creation_date_is_approximate) {
		this.creation_date_is_approximate = creation_date_is_approximate;
	}

	public Character getCreation_date_is_before() {
		return creation_date_is_before;
	}

	public void setCreation_date_is_before(Character creation_date_is_before) {
		this.creation_date_is_before = creation_date_is_before;
	}

	public Character getCreation_date_is_after() {
		return creation_date_is_after;
	}

	public void setCreation_date_is_after(Character creation_date_is_after) {
		this.creation_date_is_after = creation_date_is_after;
	}

	public String getCreation_date_territory_code() {
		return creation_date_territory_code;
	}

	public void setCreation_date_territory_code(String creation_date_territory_code) {
		this.creation_date_territory_code = creation_date_territory_code;
	}

	public String getCreation_date_location_description() {
		return creation_date_location_description;
	}

	public void setCreation_date_location_description(String creation_date_location_description) {
		this.creation_date_location_description = creation_date_location_description;
	}

	public String getCreation_date_language_and_script_code() {
		return creation_date_language_and_script_code;
	}

	public void setCreation_date_language_and_script_code(String creation_date_language_and_script_code) {
		this.creation_date_language_and_script_code = creation_date_language_and_script_code;
	}

	public Character getIs_updated() {
		return is_updated;
	}

	public void setIs_updated(Character is_updated) {
		this.is_updated = is_updated;
	}

	public String getLanguage_and_script_code() {
		return language_and_script_code;
	}

	public void setLanguage_and_script_code(String language_and_script_code) {
		this.language_and_script_code = language_and_script_code;
	}

	public String getTitle_text() {
		return title_text;
	}

	public void setTitle_text(String title_text) {
		this.title_text = title_text;
	}

	public String getSub_title() {
		return sub_title;
	}

	public void setSub_title(String sub_title) {
		this.sub_title = sub_title;
	}

	public String getResource_code() {
		return resource_code;
	}

	public void setResource_code(String resource_code) {
		this.resource_code = resource_code;
	}

	public ReleaseResources getReleaseResources() {
		return releaseResources;
	}

	public void setReleaseResources(ReleaseResources releaseResources) {
		this.releaseResources = releaseResources;
	}

	@Override
	public String toString() {
		return "Images [id=" + id + ", content_partner_id=" + content_partner_id + ", release_resource_id=" + release_resource_id + ", proprietary_id=" + proprietary_id + ", image_type_id=" + image_type_id + ", is_artist_related=" + is_artist_related + ", resource_reference=" + resource_reference + ", creation_date=" + creation_date + ", creation_date_is_approximate=" + creation_date_is_approximate + ", creation_date_is_before=" + creation_date_is_before + ", creation_date_is_after="
				+ creation_date_is_after + ", creation_date_territory_code=" + creation_date_territory_code + ", creation_date_location_description=" + creation_date_location_description + ", creation_date_language_and_script_code=" + creation_date_language_and_script_code + ", is_updated=" + is_updated + ", language_and_script_code=" + language_and_script_code + ", title_text=" + title_text + ", sub_title=" + sub_title + ", resource_code=" + resource_code + ", releaseResources="
				+ releaseResources + "]";
	}

	@Override
	public Images clone() throws CloneNotSupportedException {
		return (Images) super.clone();
	}

	public ImageTypes getImageTypes() {
		return imageTypes;
	}

	public void setImageTypes(ImageTypes imageTypes) {
		this.imageTypes = imageTypes;
	}

}
