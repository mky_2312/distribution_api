package com.spice.distribution.cms.entity;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.spice.distribution.request.Request;
import com.spice.distribution.response.Response;

//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "responseStatus", "timestamp", "data", "code" })
public class SafariComServicesResponse implements Response{

	@JsonProperty("responseStatus")
	private ResponseStatus responseStatus;
	
	@JsonProperty("timestamp")
	private BigInteger timestamp;
	
	@JsonProperty("trackingId")
	private BigInteger trackingId;
	
	@JsonProperty("data")
	private Data data;
	
	@JsonProperty("code")
	private String code;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/*======================= Getter & Setter ========================*/
	@JsonProperty("responseStatus")
	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@JsonProperty("responseStatus")
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	@JsonProperty("timestamp")
	public BigInteger getTimestamp() {
		return timestamp;
	}

	@JsonProperty("timestamp")
	public void setTimestamp(BigInteger timestamp) {
		this.timestamp = timestamp;
	}
	
	@JsonProperty("trackingId")
	public BigInteger getTrackingId() {
		return trackingId;
	}
	@JsonProperty("trackingId")
	public void setTrackingId(BigInteger trackingId) {
		this.trackingId = trackingId;
	}

	@JsonProperty("data")
	public Data getData() {
		return data;
	}

	@JsonProperty("data")
	public void setData(Data data) {
		this.data = data;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	@JsonProperty("code")
	public void setCode(String code) {
		this.code = code;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "SafariComServicesResponse [responseStatus=" + responseStatus + ", timestamp=" + timestamp + ", data=" + data + ", code=" + code + ", additionalProperties=" + additionalProperties + "]";
	}

}