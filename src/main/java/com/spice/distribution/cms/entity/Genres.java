package com.spice.distribution.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author ankit
 *
 */
@Entity
@Table(name = "Genres")
public class Genres {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String genre;
	@Column
	private int genre_type_id;
	private String genre_image_resource_code;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getGenre_type_id() {
		return genre_type_id;
	}

	public void setGenre_type_id(int genre_type_id) {
		this.genre_type_id = genre_type_id;
	}

	public String getGenre_image_resource_code() {
		return genre_image_resource_code;
	}

	public void setGenre_image_resource_code(String genre_image_resource_code) {
		this.genre_image_resource_code = genre_image_resource_code;
	}

	@Override
	public String toString() {
		return "Genres [id=" + id + ", genre=" + genre + ", genre_type_id=" + genre_type_id + "]";
	}

}
