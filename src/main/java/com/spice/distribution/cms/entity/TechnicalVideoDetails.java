package com.spice.distribution.cms.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TechnicalVideoDetails implements Cloneable {
	@Id
	private int id;

	@Column
	private BigDecimal overall_bit_rate;
	@Column
	private String unit_of_overall_bit_rate;

	private String video_codec_type;
	@Column
	private String video_codec_type_version;
	@Column
	private String video_codec_type_namespace;
	@Column
	private String video_codec_type_user_defined_value;
	@Column
	private Long video_bit_rate;
	@Column
	private String unit_of_video_bit_rate;
	@Column
	private String frame_rate;
	@Column
	private String unit_of_frame_rate;
	@Column
	private Integer height;
	@Column
	private String height_unit_of_measure;
	@Column
	private Integer width;
	@Column
	private String width_unit_of_measure;
	@Column
	private String aspect_ratio;
	@Column
	private String aspect_ratio_type;
	@Column
	private BigInteger color_depth;
	@Column
	private String video_definition_type;
	@Column
	private String audio_codec_type;

	@Column
	private Long audio_bit_rate;
	@Column
	private String unit_of_audio_bit_rate;
	@Column
	private Integer number_of_audio_channels;
	@Column
	private String audio_sampling_rate;
	@Column
	private String unit_of_audio_sampling_rate;
	@Column
	private BigInteger audio_bits_per_sample;
	@Column
	private Integer duration;
	@Column
	private Character is_preview;
	@Column
	private String expression_type;
	@Column
	private String fulfillment_date;
	@Column
	private String consumer_fulfillment_date;
	@Column
	private String language_and_script_code;
	@Column
	private String video_codec_long_name;
	@Column
	private String audio_codec_long_name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BigDecimal getOverall_bit_rate() {
		return overall_bit_rate;
	}

	public void setOverall_bit_rate(BigDecimal overall_bit_rate) {
		this.overall_bit_rate = overall_bit_rate;
	}

	public String getUnit_of_overall_bit_rate() {
		return unit_of_overall_bit_rate;
	}

	public void setUnit_of_overall_bit_rate(String unit_of_overall_bit_rate) {
		this.unit_of_overall_bit_rate = unit_of_overall_bit_rate;
	}

	public String getVideo_codec_type() {
		return video_codec_type;
	}

	public void setVideo_codec_type(String video_codec_type) {
		this.video_codec_type = video_codec_type;
	}

	public String getVideo_codec_type_version() {
		return video_codec_type_version;
	}

	public void setVideo_codec_type_version(String video_codec_type_version) {
		this.video_codec_type_version = video_codec_type_version;
	}

	public String getVideo_codec_type_namespace() {
		return video_codec_type_namespace;
	}

	public void setVideo_codec_type_namespace(String video_codec_type_namespace) {
		this.video_codec_type_namespace = video_codec_type_namespace;
	}

	public String getVideo_codec_type_user_defined_value() {
		return video_codec_type_user_defined_value;
	}

	public void setVideo_codec_type_user_defined_value(String video_codec_type_user_defined_value) {
		this.video_codec_type_user_defined_value = video_codec_type_user_defined_value;
	}

	public Long getVideo_bit_rate() {
		return video_bit_rate;
	}

	public void setVideo_bit_rate(Long video_bit_rate) {
		this.video_bit_rate = video_bit_rate;
	}

	public String getUnit_of_video_bit_rate() {
		return unit_of_video_bit_rate;
	}

	public void setUnit_of_video_bit_rate(String unit_of_video_bit_rate) {
		this.unit_of_video_bit_rate = unit_of_video_bit_rate;
	}

	public String getFrame_rate() {
		return frame_rate;
	}

	public void setFrame_rate(String frame_rate) {
		this.frame_rate = frame_rate;
	}

	public String getUnit_of_frame_rate() {
		return unit_of_frame_rate;
	}

	public void setUnit_of_frame_rate(String unit_of_frame_rate) {
		this.unit_of_frame_rate = unit_of_frame_rate;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getHeight_unit_of_measure() {
		return height_unit_of_measure;
	}

	public void setHeight_unit_of_measure(String height_unit_of_measure) {
		this.height_unit_of_measure = height_unit_of_measure;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public String getWidth_unit_of_measure() {
		return width_unit_of_measure;
	}

	public void setWidth_unit_of_measure(String width_unit_of_measure) {
		this.width_unit_of_measure = width_unit_of_measure;
	}

	public String getAspect_ratio() {
		return aspect_ratio;
	}

	public void setAspect_ratio(String aspect_ratio) {
		this.aspect_ratio = aspect_ratio;
	}

	public String getAspect_ratio_type() {
		return aspect_ratio_type;
	}

	public void setAspect_ratio_type(String aspect_ratio_type) {
		this.aspect_ratio_type = aspect_ratio_type;
	}

	public BigInteger getColor_depth() {
		return color_depth;
	}

	public void setColor_depth(BigInteger color_depth) {
		this.color_depth = color_depth;
	}

	public String getVideo_definition_type() {
		return video_definition_type;
	}

	public void setVideo_definition_type(String video_definition_type) {
		this.video_definition_type = video_definition_type;
	}

	public String getAudio_codec_type() {
		return audio_codec_type;
	}

	public void setAudio_codec_type(String audio_codec_type) {
		this.audio_codec_type = audio_codec_type;
	}

	public Long getAudio_bit_rate() {
		return audio_bit_rate;
	}

	public void setAudio_bit_rate(Long audio_bit_rate) {
		this.audio_bit_rate = audio_bit_rate;
	}

	public String getUnit_of_audio_bit_rate() {
		return unit_of_audio_bit_rate;
	}

	public void setUnit_of_audio_bit_rate(String unit_of_audio_bit_rate) {
		this.unit_of_audio_bit_rate = unit_of_audio_bit_rate;
	}

	public Integer getNumber_of_audio_channels() {
		return number_of_audio_channels;
	}

	public void setNumber_of_audio_channels(Integer number_of_audio_channels) {
		this.number_of_audio_channels = number_of_audio_channels;
	}

	public String getAudio_sampling_rate() {
		return audio_sampling_rate;
	}

	public void setAudio_sampling_rate(String audio_sampling_rate) {
		this.audio_sampling_rate = audio_sampling_rate;
	}

	public String getUnit_of_audio_sampling_rate() {
		return unit_of_audio_sampling_rate;
	}

	public void setUnit_of_audio_sampling_rate(String unit_of_audio_sampling_rate) {
		this.unit_of_audio_sampling_rate = unit_of_audio_sampling_rate;
	}

	public BigInteger getAudio_bits_per_sample() {
		return audio_bits_per_sample;
	}

	public void setAudio_bits_per_sample(BigInteger audio_bits_per_sample) {
		this.audio_bits_per_sample = audio_bits_per_sample;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Character getIs_preview() {
		return is_preview;
	}

	public void setIs_preview(Character is_preview) {
		this.is_preview = is_preview;
	}

	public String getExpression_type() {
		return expression_type;
	}

	public void setExpression_type(String expression_type) {
		this.expression_type = expression_type;
	}

	public String getFulfillment_date() {
		return fulfillment_date;
	}

	public void setFulfillment_date(String fulfillment_date) {
		this.fulfillment_date = fulfillment_date;
	}

	public String getConsumer_fulfillment_date() {
		return consumer_fulfillment_date;
	}

	public void setConsumer_fulfillment_date(String consumer_fulfillment_date) {
		this.consumer_fulfillment_date = consumer_fulfillment_date;
	}

	public String getLanguage_and_script_code() {
		return language_and_script_code;
	}

	public void setLanguage_and_script_code(String language_and_script_code) {
		this.language_and_script_code = language_and_script_code;
	}

	public String getVideo_codec_long_name() {
		return video_codec_long_name;
	}

	public void setVideo_codec_long_name(String video_codec_long_name) {
		this.video_codec_long_name = video_codec_long_name;
	}

	public String getAudio_codec_long_name() {
		return audio_codec_long_name;
	}

	public void setAudio_codec_long_name(String audio_codec_long_name) {
		this.audio_codec_long_name = audio_codec_long_name;
	}

	@Override
	public TechnicalVideoDetails clone() throws CloneNotSupportedException {
		return (TechnicalVideoDetails) super.clone();
	}

}