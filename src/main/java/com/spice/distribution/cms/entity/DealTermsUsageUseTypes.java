package com.spice.distribution.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DealTermsUsageUseTypes")
public class DealTermsUsageUseTypes {
	@Id
	private int id;
	
	@Column(name="deal_id")
	private Integer dealId;
	@Column(name="use_type_id")
	private Integer useTypeId;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the dealId
	 */
	public Integer getDealId() {
		return dealId;
	}
	/**
	 * @param dealId the dealId to set
	 */
	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}
	/**
	 * @return the useTypeId
	 */
	public Integer getUseTypeId() {
		return useTypeId;
	}
	/**
	 * @param useTypeId the useTypeId to set
	 */
	public void setUseTypeId(Integer useTypeId) {
		this.useTypeId = useTypeId;
	}
	
	

}
